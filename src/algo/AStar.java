package algo;
import java.util.*;

import config.robotConfig.DIRECTION;
import config.robotConfig.MOVEMENT;
import config.settings;
import map.Map;
import robot.robot;
/*
 *  Fastest Pathfinding Algorithm for CZ3004 Multidisciplinary Project 
 *  Version 1.3 
 *  For Group 19
 *  By: Javier Png 
 * 
 *  This A* algorithm prefers straight line than hypo, hence lesser turning = fastest path possible.
 *  Algorithm added bias to decide between northwards or eastwards.
 *  Virtual Boundary Cells : 12 13 14 29 44 turned off so that robot can turn. Refer to the picture in google drive.
 *  
 *  Main Method is needed to run standalone.
 *  
 *  Usage:
 *  	beginPathFinding(begin(String bitstream));
 *  	beginPathFinding(begin()); for manual wall input 
 *  Returns:
 *  	int[], where even indexes are x-coordinates and odd indexes are y-coordinates; index 0 and 1 is the current position
 */
import tools.communicator;
import tools.communicator.COMMANDS;
import robot.robot;

class Cell{	
	// Each cell contains the location of the neighbor.
	int pos = 0;
	int north = 0;
	int east = 0;
	int west = 0;
	int south = 0;
	int dist = 0;
	double heuristic = 99999;
	int camefrom = -1;
	
}

class compareTo implements Comparator<Cell>{
	public int compare(Cell arg0, Cell arg1) {
		if( arg0.heuristic - arg1.heuristic > 0) {
			return 1;
		}
		return 0;
	}
}

public class AStar {
	static Cell [] grid = new Cell[300];
	static int [] closedSet = new int [300];
	static Scanner sc = new Scanner(System.in);
	static int startLoc = -1, endLoc = -1;
	static double bias = 0;
	static robot r = new robot(new int[] {1,1}, false);
	static boolean isRunning = false;
	//Initializing new cells
	private static Cell createCell(int pos, int north, int east, int west, int south, int dist) {
		Cell c = new Cell();
		c.pos = pos;
		c.north = north;
		c.east = east;
		c.west = west;
		c.south = south;
		c.dist = dist;
		c.camefrom = -1;
		return c;
	}
	
	//Populating Grid
	private static void loadGrid() {
		for(int i = 0; i < 300; i++) {
			if(i == 0) {
				grid[i] = createCell(i, -1, 1, -1, 15, 0);
			}
			else if(i == 14) {
				grid[i] = createCell(i, -1, -1, 13, 29, 0);
			}
			else if(i == 285) {
				grid[i] = createCell(i, 270, 286, -1, -1, 0);
			}
			else if(i == 299) {
				grid[i] = createCell(i, 284, -1, 298, -1, 0);
			}
			else if(i < 14) {
				grid[i] = createCell(i, -1, i+1, i-1, i+15, 0);
			}
			else if(i > 285) {
				grid[i] = createCell(i, i-15, i+1, i-1, -1, 0);
			}
			else if(i % 15 == 0) {
				grid[i] = createCell(i, i-15, i+1, -1, i+15, 0);
			}
			else if(i % 15 == 14) {
				grid[i] = createCell(i, i-15, -1, i-1, i+15, 0);
			}
			else {
				grid[i] = createCell(i, i-15, i+1, i-1, i+15, 0);
			}
		}
		//Default Virtual Boundaries
		for(int i = 0; i < 20; i ++) {
			closedSet[i*15] = -1;
			closedSet[i*15+14] = -1;
			if(i < 15) {
				closedSet[i] = -1;
				closedSet[i + 285] = -1;
			}
		}
		System.out.println("Virtual Ready!");
	}
	
	private static String execute(int[] xy) {
		String res = "";
		for(int i = 2; i < xy.length; i+=2) {
		}
		return res;
	}
	
	//Path printing, then reverse store array to be returned.
	private static int[] reconstruction(Cell goal) {
		int[] reconst = new int[(goal.dist + 1)*2];
		String path = goal.pos + "";
		int count = (goal.dist+ 1)*2 - 1;
		System.out.println("Shortest Distance: " + goal.dist);
		while(goal.camefrom != -1) {
			reconst[count-1] = goal.pos % 15 ;
			reconst[count] = goal.pos/15;
			count -= 2;
			goal = grid[goal.camefrom];
			path = goal.pos + " -> " + path;
		}
		reconst[0] = goal.pos % 15 ;
		reconst[1] = goal.pos /15;
		System.out.println(path);
		execute(reconst);
		return reconst;
	}
	
	public static ArrayList<Integer> convertForSim(int[] xy) {
		ArrayList<Integer> res = new ArrayList<Integer>();
		for(int i = 0; i < xy.length; i += 2)
		{
			res.add(xy[i+1]*15 + xy[i]);
		}
		return res;
	}
	
	private static double calculateCost(int position) {
		// Manhattan Distance Heuristic that prefers straight line movements.
		double heu = Math.abs((position * 1.0 % 15) - (endLoc * 1.0 % 15)) + Math.abs((position * 1.0 / 20) - (endLoc * 1.0/ 20));
		return heu;
	}
	
	public static ArrayList<Integer> begin() {
		ArrayList<Integer> al = new ArrayList<Integer>();
		int t = 0;
		while(t != -1) {
			System.out.print("Enter wall, -1 to end: ");
			t = sc.nextInt();
			if(t > 0)
				al.add(t);
		}
		return al;
	}
	
	public static ArrayList<Integer> begin(String args) {
		String[] a = args.split("(?!^)");
		int counterS = 0, counterE = 0;
		int rowCounter = 0;
		
		ArrayList<Integer> al = new ArrayList<Integer>();
		for(int i = 2 ; i < a.length-2; i++) {
			if(Integer.parseInt(a[i]) == 1) {
				System.out.print((i-2) + " ");
				al.add(i-2);
				rowCounter += 1;
				if(rowCounter == 14) {
					System.out.println("");
					rowCounter = 0;
				}
			}
			if(Integer.parseInt(a[i]) == 4 && counterE == 4) {
				endLoc = i-2;
				counterE = 99;
			}
			else if(Integer.parseInt(a[i]) == 4){
				counterE += 1;
			}
			if(Integer.parseInt(a[i]) == 5 && counterS == 4) {
				startLoc = i-2;
				counterS = 99;
			}
			else if(Integer.parseInt(a[i]) == 5){
				counterS += 1;
			}
		}
		return al;
	}
	
	public static int[] beginPathfinding(Map exploredMap, int start, int end) {
		ArrayList<Cell> openSet = new ArrayList<Cell>();
		ArrayList<Integer> walls = exploredMap.findWalls();
		loadGrid();
		bias = 0;
		if(start != -1) {
			startLoc = start;
		}
		if(end != -1) {
			endLoc = end;
		}
		int[] inQueue = new int[300];
		int counter = 0;
		for(int i = 0; i < walls.size(); i++) {
			Cell getCell = grid[(int)walls.get(i)];
			// check the distribution of obstacles in the map
			if(getCell.pos > 150) {
				bias += 1;
			}
			if(getCell.north != -1) {
                closedSet[getCell.north] = -1;
                if(getCell.north -1 > 0)
                	closedSet[getCell.north - 1] = -1;
                if(getCell.north + 1 < 300)
                	closedSet[getCell.north + 1] = -1;
			}
            if(getCell.east != -1) {
                closedSet[getCell.east] = -1;
            }
            if(getCell.west != -1) {
                closedSet[getCell.west] = -1;
            }
            if(getCell.south != -1) {
                closedSet[getCell.south] = -1;
                closedSet[getCell.south -1] = -1;
                closedSet[getCell.south +1] = -1;
            }
			closedSet[getCell.pos] = -1;
		}
		bias = bias/walls.size(); //get proportion
		System.out.println("Walls and Virtual Boundaries added! Bias: " + bias);
		if(startLoc == -1) {
			System.out.print("Enter Start Location: ");
			startLoc = sc.nextInt();
		}
		if(endLoc == -1) {
			System.out.println(endLoc + " , " + startLoc);
			System.out.print("Enter Goal Location: ");
			endLoc = sc.nextInt();
		}
		System.out.println("FS: "  + startLoc + " FE: " + endLoc);
		grid[startLoc].heuristic = 0;
		openSet.add(grid[startLoc]);
		while(!openSet.isEmpty()) {
			Cell c = openSet.remove(0);
			counter += 1;
			inQueue[c.pos] = 0;
			closedSet[c.pos] = -1;
			if(c.pos == endLoc) {
				System.out.println("Found!\nExplored Cells: " + counter);
				closedSet = new int[300];
				return reconstruction(c);
			}
			if(c.south != -1 && closedSet[c.south] != -1 && inQueue[c.south] == 0) {
	        	grid[c.south].dist = c.dist + 1;
	            grid[c.south].heuristic = calculateCost(c.south) + c.dist + 1;
	            openSet.add(grid[c.south]);
	            inQueue[c.south] = 1;
	            grid[c.south].camefrom = c.pos;
	        }
			
			if(c.east != -1 && closedSet[c.east] != -1 && inQueue[c.east] == 0) {
	        	grid[c.east].dist = c.dist + 1;
	            grid[c.east].heuristic = calculateCost(c.east) + c.dist + 1;
	            openSet.add(grid[c.east]);
	            inQueue[c.east] = 1;
	            grid[c.east].camefrom = c.pos;
	        } 
			if(c.north != -1 && closedSet[c.north] != -1 && inQueue[c.north] == 0) {
	            grid[c.north].dist = c.dist + 1;
	            grid[c.north].heuristic = calculateCost(c.north) + c.dist + 1;
	            openSet.add(grid[c.north]);
	            inQueue[c.north] = 1;
	            grid[c.north].camefrom = c.pos;
			}	
	        if(c.west != -1 && closedSet[c.west] != -1 && inQueue[c.west] == 0) {
	        	grid[c.west].dist = c.dist + 1;
	            grid[c.west].heuristic = calculateCost(c.west) + c.dist + 1;
	            openSet.add(grid[c.west]);
	            inQueue[c.west] = 1;
	            grid[c.west].camefrom = c.pos;
	        }
	        Collections.sort(openSet, new compareTo());
		}
		closedSet = new int[300];
		System.out.println("No path found! Ensure if your start/end point is not on virtual boundary.");
		return new int[] {};
	}
	
	public static boolean moveRobot(Map exploredMap,Map realmap, robot bot, int start, int end,boolean sensor,boolean fastest) {
		String arrayofcommands = "";
		int col = start / 15, row = start % 15;
	    int[] path = null;
	    int[] coord = {col, row};
	    ArrayList<map.Cell> waypoints = exploredMap.findWaypoints();
	    if(waypoints.size() > 0 && fastest) {
	      int current = start;
	      int temp[] = {1,1};
	      while(waypoints.size() != 0) {
	        System.out.println(waypoints.size());
	        map.Cell c = waypoints.remove(0);
	        int[] p  =  AStar.beginPathfinding(exploredMap, current, c.getID());
	        if(p.length > 0) {
	          int[] p2 = new int[p.length + temp.length-2];
	          System.arraycopy(temp, 0, p2, 0, temp.length);
	          System.arraycopy(p, 0, p2, temp.length-2, p.length);
	          current = c.getID();
	          temp = p2;
	        }
	        else {
	        	continue;
	        }
	      }
	      int[] p  =  AStar.beginPathfinding(exploredMap, current, end);
	      if(p.length > 0 ) {
	        int[] p2 = new int[p.length-2 + temp.length];
	        System.arraycopy(temp, 0, p2, 0, temp.length);
	        System.arraycopy(p, 0, p2, temp.length-2, p.length);
	        path = p2;
	      }
	    }
	    else {
	      path = AStar.beginPathfinding(exploredMap, start, end);
	    }
	    System.out.println("A* : " + path.length);
	    if(path.length == 0) return false;
	    bot.setPos(coord);
	    //bot.setRobotDirection(DIRECTION.NORTH);
	    col = path[0];
	    row = path[1];
	    for(int i = 2 ; i < path.length-1; i +=2) {
		    //if(settings.END_TIMESTAMP - System.currentTimeMillis() > 0) {
		      System.out.println("Col: " + path[i]+ ", Row: " + path[i+1]);
		      int counter = 0;
		      if(col == path[i]) {
		        if(row > path[i+1]) {
		        	if(bot.getReal())
		        		arrayofcommands += turnBotDirectionReal(DIRECTION.SOUTH,bot);
		        	else
		        		turnBotDirection(DIRECTION.SOUTH,bot);
		        }
		        else {
		        	if(bot.getReal())
		        		arrayofcommands += turnBotDirectionReal(DIRECTION.NORTH,bot);
		        	else
		        		turnBotDirection(DIRECTION.NORTH, bot);
		        }
		        //Problem here does it applies to fastest and realBot or fake?
		        if(fastest) {
		        	for(int step = 0; step < 10; step++) {
		        		if(i + (step*2) >= path.length) {
		        			if(counter == 0) counter = 1;
		        			break; 
		        		}
		        		int nextVal = path[i+(step*2)];
		        		if(nextVal == col) {
		        			counter += 1;
		        		}
		        		else break;
		        	}
		        	arrayofcommands += getForwardMultiple(counter);
		        	bot.moveForwardMultiple(counter,true);
		        }
		        else {
		        	//Cos here u're resetting it back to 1
		        	counter = 1;
		        	bot.moveForwardMultiple(counter,false);
		        }
	        	i += (counter-1) * 2;
	        	col = path[i];
		        row = path[i+1];
		        System.out.println("counter reported: " + counter);
		        counter = 0;
		        setRepaint(bot,sensor,exploredMap,realmap);
		        exploredMap.repaint();
		        continue;
		      }
		      else{ // y == path[i+1]
		        if(col > path[i]) {
		        	arrayofcommands += turnBotDirectionReal(DIRECTION.WEST,bot);
		        	if(bot.getReal())
		        		arrayofcommands += turnBotDirectionReal(DIRECTION.WEST,bot);
		        	else
		        		turnBotDirection(DIRECTION.WEST, bot);
		        }
		        else {
		        	arrayofcommands += turnBotDirectionReal(DIRECTION.EAST,bot);
		        	if(bot.getReal())
		        		arrayofcommands += turnBotDirectionReal(DIRECTION.EAST,bot);
		        	else
		        		turnBotDirection(DIRECTION.EAST, bot);
		        }
		        //Same Problem lies here
		        if(fastest) {
		        	for(int step = 0; step < 10; step++) {
		        		if(i + (step*2) >= path.length) {
		        			if(counter == 0) counter = 1;
		        			break; 
		        		}
		        		int nextVal = path[i+1+(step*2)];
		        		if(nextVal == row) {
		        			counter += 1;
		        		}
		        		else break;
		        	}
		        	arrayofcommands += getForwardMultiple(counter);
		        	bot.moveForwardMultiple(counter,true);
		        }
		        else {
		        	counter = 1;
		        	bot.moveForwardMultiple(counter,false);
		        	
		        }
	        	i += (counter-1) * 2;
	        	col = path[i];
		        row = path[i+1];
		        System.out.println("counter reported: " + counter);
	        	counter = 0;
		        setRepaint(bot,sensor,exploredMap,realmap);
		        exploredMap.repaint();
		        continue;
		      }
		    //}
		    //else break;
	    }
	    
	    System.out.println(arrayofcommands);
	    if(bot.getReal())
	    	communicator.getCommunicator().sendMsg(arrayofcommands, COMMANDS.INSTRUCTIONS, false);
	    return true;
	    }
	
	private static void setRepaint(robot bot,boolean sensor,Map exploredMap,Map realmap)
	{
		if(sensor)
		{
			bot.setSensors();
			bot.sense(exploredMap,realmap);
			exploredMap.repaint();	
		}
	}
	
	private static String turnBotDirectionReal(DIRECTION targetDir,robot bot) {
        int numOfTurn = Math.abs(bot.getRobotDirection().ordinal() - targetDir.ordinal());
        String instruction = "";
        
        if (numOfTurn > 2) numOfTurn = numOfTurn % 2;

        if (numOfTurn == 1) {
        	System.out.println(DIRECTION.getNextDirection(bot.getRobotDirection()) + " : " + targetDir);
            if (DIRECTION.getNextDirection(bot.getRobotDirection()) == targetDir) {
            	instruction += "d";
            	bot.setRobotDirection(targetDir);
            } else {
            	instruction += "a";
            	bot.setRobotDirection(targetDir);
            }
        } else if (numOfTurn == 2) {
        	instruction += "dd";
        	bot.setRobotDirection(targetDir);
        }
        return instruction;
    }
	
    private static void turnBotDirection(DIRECTION targetDir,robot bot) {
        int numOfTurn = Math.abs(bot.getRobotDirection().ordinal() - targetDir.ordinal());
        if (numOfTurn > 2) numOfTurn = numOfTurn % 2;

        if (numOfTurn == 1) {
            if (DIRECTION.getNextDirection(bot.getRobotDirection()) == targetDir) {
                bot.robotMovement(MOVEMENT.RIGHT);
            } else {
            	bot.robotMovement(MOVEMENT.LEFT);
            }
        } else if (numOfTurn == 2) {
        	bot.robotMovement(MOVEMENT.RIGHT);
        	bot.robotMovement(MOVEMENT.RIGHT);
        }
    }
	
	private static String getForwardMultiple(int count)
	{
		switch(count)
    	{
			case 1:
				return "w";
        	case 2:
        		return "q";
        	case 3:
        		return "e";
        	case 4:
        		return "r";
        	case 5:
        		return "t";
        	case 6:
        		return "y";
        	case 7:
        		return "u";
        	case 8:
        		return "i";
        	case 9:
        		return "o";
        	case 10:
        		return "p";
    	}
		return "";
	}
}
