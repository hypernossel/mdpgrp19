package algo;


import map.Cell;
import map.Map;
import robot.*;
import tools.communicator;
import tools.communicator.COMMANDS;
import tools.utils;

import java.util.ArrayList;

import UI.Window;
import algo.AStar;
import config.config;
import config.robotConfig;
import config.robotConfig.DIRECTION;
import config.robotConfig.MOVEMENT;
import config.settings;

public class MapExploration {
	private final Map exploredMap;
    private final robot bot;
    private int coverageLimit;
    private int timeLimit;
    private int areaExplored;
    private long startTime;
    private boolean isExploring = false;
    private String output = "";
    private ArrayList<Cell> unexploredWaypoints;
    private boolean calibrationState;
    private int lastCalibrate;
    private int current_Threshold = 1;
    private final int grid_Threshold = 3;
    
    private int unexploredThreshold = 0;
    private int prev_unexploredSize = 0;
    public static boolean isPause = false;
    
    private boolean a_star_state = false;
    
    int move_threshold = 0,forward_threshold =0;
    
    private final Map realMap;
    
    public MapExploration(Map exploredMap,Map realMap, robot bot, int coverageLimit)
    {
    	this.exploredMap = exploredMap;
        this.bot = bot;
        this.realMap = realMap;
        this.coverageLimit = coverageLimit;
    }
    
    public boolean getIsExploring() {return this.isExploring;}
    public void setIsExploring(boolean state) {this.isExploring = state;}
    
    public void setTimeLimit(int limit) { this.timeLimit = limit;}
    public void setCoverageLimit(int limit) {this.coverageLimit = limit;}
    
    public String getOutputString() { return this.output; }
    
    public void run() throws InterruptedException
    {

    	if(bot.getReal())
    	{
    		communicator.getCommunicator().recvMsg();
    		//Without Motor Test
    		String waypoint  = communicator.getCommunicator().recvMsg();
    		System.out.println(waypoint);
            if(waypoint.substring(0, 8).equals("WAYPOINT")) {
            	String[] _waypoint= waypoint.split(":")[1].split(",");
            	exploredMap.setWayPoint(Integer.parseInt(_waypoint[0]), Integer.parseInt(_waypoint[1]), true);
            	System.out.println("Waypoint Recieved");
            }
            
    		//Robot Initializing
    		bot.setInitializing(true);
    		System.out.println("Starting Calibration....");
    		//Sending calibration chunk
    		bot.robotMovement(MOVEMENT.LEFT);
    		communicator.getCommunicator().recvMsg();
    		bot.robotMovement(MOVEMENT.CALIBRATION);
    		communicator.getCommunicator().recvMsg();
    		bot.robotMovement(MOVEMENT.LEFT);
    		communicator.getCommunicator().recvMsg();
    		bot.robotMovement(MOVEMENT.CALIBRATION);
    		communicator.getCommunicator().recvMsg();
    		bot.robotMovement(MOVEMENT.RIGHT,false);
    		communicator.getCommunicator().recvMsg();
    		bot.robotMovement(MOVEMENT.RIGHT,false);
            bot.setInitializing(false);
            
            if(communicator.getCommunicator().recvMsg().equals("EX_START"));
    	}
    	
    	System.out.println("Starting exploration...");
    	
    	settings.END_TIMESTAMP = System.currentTimeMillis() + settings.TIME_LIMIT;
        
        senseAndRepaint();
        
        areaExplored = calculateAreaExplored();
        
        System.out.println("Explored Area: " + areaExplored);
    	System.out.println(bot.getRobotPos()[0] +","+ bot.getRobotPos()[1]);
    	explorationLoop(bot.getRobotPos());
    	isExploring = true;
    	
    	communicator.getCommunicator().sendMsg(null, COMMANDS.BOT_START, true);
    }
    
    /**
     * Loops through robot movements until one (or more) of the following conditions is met:
     * 1. Robot is back at (r, c)
     * 2. areaExplored > coverageLimit
     * 3. System.currentTimeMillis() > endTime
     */
    private void explorationLoop(int[] pos) {
        do {
            nextMove();
            areaExplored = calculateAreaExplored();
            System.out.println("Area explored: " + areaExplored);
            if (bot.getRobotPos()[0] == robotConfig.START_ROW && bot.getRobotPos()[1] == robotConfig.START_COL) {
            	if(areaExplored >= 70) {
            		break;
            	}
            }
            System.out.println("FACING : " + bot.getRobotDirection());
            int seconds = (int)((settings.END_TIMESTAMP - System.currentTimeMillis()) / 1000.0);
            System.out.println(utils.generateMDF(exploredMap));
            
            Window.labelStatus.setText("TIME: " + seconds);
        } while (areaExplored <= coverageLimit && System.currentTimeMillis() <= settings.END_TIMESTAMP);
        System.out.println(areaExplored + " : " + coverageLimit);
        
        
        //Wait for Calibration to go back to start
        if(areaExplored < coverageLimit && System.currentTimeMillis() <= settings.END_TIMESTAMP ) {	
        	getUnExploredPath();
        }

        returnToStart();
    }
    
    /**
     * Determines the next move for the robot and executes it accordingly.
     */
    private void nextMove() {
    	if (facingRight()) {
            moveBot(MOVEMENT.RIGHT);
            move_threshold++;
            forward_threshold = 0;
            System.out.println("MOVE THRESHOLD" + move_threshold);
            if(move_threshold == 3)
            {
            	firstMove();
            	move_threshold = 0;
            }
            if (facingforward()) {
            	moveBot(MOVEMENT.FORWARD);
            }
        } else if (facingforward()) {
            moveBot(MOVEMENT.FORWARD);
            forward_threshold++;
        	if(forward_threshold == 2)
        	{
        		move_threshold = 0;
        	}
        } else if (facingleft()) {
            moveBot(MOVEMENT.LEFT);
            
        }else
        {
        	moveBot(MOVEMENT.RIGHT);
        }
    }
    
    private void firstMove()
    {
    	while(facingforward())
    	{
    		moveBot(MOVEMENT.FORWARD);
    		
    	}
    	moveBot(MOVEMENT.LEFT);
    }
    
    /**
     * Returns true if the right side of the robot is free to move into.
     */
    private boolean facingRight() {
        switch (bot.getRobotDirection()) {
            case NORTH:
                return eastFree();
            case EAST:
                return southFree();
            case SOUTH:
                return westFree();
            case WEST:
                return northFree();
        }
        return false;
    }
    
    /**
     * Returns true if the robot is free to move forward.
     */
    private boolean facingforward() {
        switch (bot.getRobotDirection()) {
            case NORTH:
                return northFree();
            case EAST:
                return eastFree();
            case SOUTH:
                return southFree();
            case WEST:
                return westFree();
        }
        return false;
    }
    
    /**
     * * Returns true if the left side of the robot is free to move into.
     */
    private boolean facingleft() {
        switch (bot.getRobotDirection()) {
            case NORTH:
                return westFree();
            case EAST:
                return northFree();
            case SOUTH:
                return eastFree();
            case WEST:
                return southFree();
        }
        return false;
    }
    
    /**
     * Returns true if the robot can move to the north cell.
     */
    private boolean northFree() {
    	int[] pos = bot.getRobotPos();
        return (isExploredNotObstacle(new int[] {pos[0]+1,pos[1]-1}) &&
        		isExploredAndFree(new int[] {pos[0]+1,pos[1]}) &&
        		isExploredNotObstacle(new int[] {pos[0]+1,pos[1]+1}));
    }

    /**
     * Returns true if the robot can move to the east cell.
     */
    private boolean eastFree() {
    	int[] pos = bot.getRobotPos();
        return (isExploredNotObstacle(new int[] {pos[0]-1,pos[1]+1}) &&
        		isExploredAndFree(new int[] {pos[0],pos[1]+1}) &&
        		isExploredNotObstacle(new int[] {pos[0]+1,pos[1]+1}));
    }

    /**
     * Returns true if the robot can move to the south cell.
     */
    private boolean southFree() {
    	int[] pos = bot.getRobotPos();
        return (isExploredNotObstacle(new int[] {pos[0]-1,pos[1]-1}) &&
        		isExploredAndFree(new int[] {pos[0]-1,pos[1]}) &&
        		isExploredNotObstacle(new int[] {pos[0]-1,pos[1]+1}));
    }

    /**
     * Returns true if the robot can move to the west cell.
     */
    private boolean westFree() {
    	int[] pos = bot.getRobotPos();
        return (isExploredNotObstacle(new int[] {pos[0]-1,pos[1]-1}) && 
        		isExploredAndFree(new int[] {pos[0],pos[1]-1}) && 
        		isExploredNotObstacle(new int[] {pos[0]+1,pos[1]-1}));
    }
    
    
    /**
     * Returns true for cells that are explored and not obstacles.
     */
    private boolean isExploredNotObstacle(int[] pos) {
        if (exploredMap.checkValidCoordinates(pos)) {
            Cell tmpCell = exploredMap.getCell(pos);
            return (tmpCell.isVisited() && !tmpCell.isObsticle());
        }
        return false;
    }
    
    /**
     * Returns true for cells that are explored, not virtual walls and not obstacles.
     */
    private boolean isExploredAndFree(int[] pos) {
        if (exploredMap.checkValidCoordinates(pos)) {
            Cell block = exploredMap.getCell(pos);
            return (block.isVisited() && !block.isVirtualWall() && !block.isObsticle());
        }
        return false;
    }
    
    /**
     * Returns the number of cells explored in the grid.
     */
    private int calculateAreaExplored() {
        int result = 0;
        for (int i = 0; i < config.GRID_ROW; i++) {
            for (int j = 0; j < config.GRID_COL; j++) {
                if (exploredMap.getCell(new int[] {i,j}).isVisited()) {
                    result++;
                }
            }
        }
        return result;
    }
    
    /**
     * Moves the bot, repaints the map and calls senseAndRepaint().
     */
    private void moveBot(MOVEMENT m) {
        bot.robotMovement(m);
        exploredMap.repaint();
        
        if(m!= MOVEMENT.CALIBRATION)
        	senseAndRepaint();
        else
        {
        	communicator comm = communicator.getCommunicator();
        	comm.recvMsg();
        }
       
        if(bot.getReal() && !calibrationState)
        {
        	calibrationState = true;
        	// REMOVE THIS LTR
        	
        	if(CalibrateOnTheSpot(bot.getRobotDirection()))
        	{
        		lastCalibrate = 0;
        		//REMOVE THIS LTR
        		moveBot(MOVEMENT.CALIBRATION);
        	}else
        	{
        		lastCalibrate++;
        		if(lastCalibrate > 8)
        		{
        			DIRECTION tarDir = getCalibrationDirection();
        			if(tarDir != null)
        			{
        				lastCalibrate = 0;
        				//REMOVE THIS LTR
        				calibrateBot(tarDir);
        			}
        		}
        	}
        }
        
        calibrationState = false;
        
    }
    
    /**
     * Sets the bot's sensors, processes the sensor data and repaints the map.
     */
    private void senseAndRepaint() {
    	exploredMap.getCell(new int[] {bot.getRobotPos()[0],bot.getRobotPos()[1]}).setTrail(true);
        bot.setSensors();
        bot.sense(exploredMap,realMap);
        exploredMap.repaint();
    }
    
    /**
     * Turns the robot to the required direction.
     */
    private void turnBotDirection(DIRECTION targetDir) {
        int numOfTurn = Math.abs(bot.getRobotDirection().ordinal() - targetDir.ordinal());
        if (numOfTurn > 2) numOfTurn = numOfTurn % 2;

        if (numOfTurn == 1) {
            if (DIRECTION.getNextDirection(bot.getRobotDirection()) == targetDir) {
                moveBot(MOVEMENT.RIGHT);
            } else {
                moveBot(MOVEMENT.LEFT);
            }
        } else if (numOfTurn == 2) {
            moveBot(MOVEMENT.RIGHT);
            moveBot(MOVEMENT.RIGHT);
        }
    }
    
    private void returnToStart()
    {
    	if (!bot.getReachGoalStatus() && coverageLimit == 300 && timeLimit == 3600) {
    		int start = (exploredMap.getMap()[robotConfig.START_ROW][robotConfig.START_COL]).getID();
    		int goal = (exploredMap.getMap()[config.GOAL_LOC[0]][config.GOAL_LOC[1]]).getID();
            AStar.beginPathfinding(exploredMap, start, goal);
        }
    	
    	bot.setRobotDirection(DIRECTION.SOUTH);
        calibrateBotWhenReachedStart(bot);
    
    	settings.END_TIMESTAMP += settings.TIME_LIMIT;
    	
    	if(!(bot.getRobotPos()[0] == robotConfig.START_ROW) && !(bot.getRobotPos()[1] == robotConfig.START_COL)) {
    	
    		AStar.moveRobot(exploredMap,
        				realMap,
        				bot,
        				(exploredMap.getMap()[bot.getRobotPos()[0]][bot.getRobotPos()[1]]).getID(),
        				(exploredMap.getMap()[robotConfig.START_ROW][robotConfig.START_COL]).getID(),false,false);
    	}

        System.out.println("Exploration complete!");
        areaExplored = calculateAreaExplored();
        output += "<html>Status<br>Maze : "+((areaExplored / 300.0) * 100.0)  + "% Coverage" + "<br>";
        output += "Explored Area : " + areaExplored + " Cells<br>";
        output += "Time Elaspe : " + (Math.abs((settings.END_TIMESTAMP - System.currentTimeMillis())) / 100000) + " Seconds<br>";
        
        System.out.printf("%.2f%% Coverage", (areaExplored / 300.0) * 100.0);
        System.out.println(", " + areaExplored + " Cells");
        System.out.println((System.currentTimeMillis() - startTime) / 1000 + " Seconds");
        isExploring = false;
        
        communicator.getCommunicator().sendMsg(null, COMMANDS.BOT_END, true);
        
        return;
    }
    
    private void getUnExploredPath()
    {
    	unexploredWaypoints = exploredMap.findUnexplored(current_Threshold);
    	do
    	{
    		if(unexploredWaypoints.size() == 0 || unexploredThreshold > 3)
    			return;
    		
    		areaExplored = calculateAreaExplored();
    		if(!a_star_state)
    			runWaypoints();
    		
    		
    	}while(unexploredWaypoints.size() > 0 && System.currentTimeMillis() <= settings.END_TIMESTAMP);
    }
    
    public void runWaypoints()
    {
    	a_star_state = false;
    	prev_unexploredSize = unexploredWaypoints.size();
    	
    	System.out.println("Searching Waypoint");
    	if(areaExplored < coverageLimit && unexploredWaypoints == null) {
    		if(current_Threshold != grid_Threshold) {
    			current_Threshold++;
    			unexploredWaypoints = exploredMap.findUnexplored(current_Threshold);
    		}
    	}
    	System.out.println("MapExplore : " + unexploredWaypoints.size());
    	Cell _cell = unexploredWaypoints.remove(0);
		int cellPos = exploredMap.getCell(new int[] {bot.getRobotPos()[0],bot.getRobotPos()[1]}).getID();
		//Problem lies here
		a_star_state = AStar.moveRobot(exploredMap,realMap, bot, cellPos, _cell.getID(),true,false);
		unexploredWaypoints = exploredMap.findUnexplored(current_Threshold);
		
		if(unexploredWaypoints.size() == prev_unexploredSize)
			unexploredThreshold++;
		
		senseAndRepaint();
    }
    
    /****** CALIBRATION PART ******/
    
    private boolean CalibrateOnTheSpot(DIRECTION botDir) {
    	System.out.println("CalibrateOnTheSpot : Re-Calibration");
        int row = bot.getRobotPos()[0];
        int col = bot.getRobotPos()[1];

        switch (botDir) {
            case NORTH:
                return exploredMap.getIsObstacleOrWall(new int[] {row + 2, col - 1}) &&
                	   exploredMap.getIsObstacleOrWall(new int[] {row + 2, col}) &&
                	   exploredMap.getIsObstacleOrWall(new int[] {row + 2, col + 1});
            case EAST:
                return exploredMap.getIsObstacleOrWall(new int[] {row + 1, col + 2}) &&
                	   exploredMap.getIsObstacleOrWall(new int[] {row, col + 2}) &&
                	   exploredMap.getIsObstacleOrWall(new int[] {row - 1, col + 2});
            case SOUTH:
                return exploredMap.getIsObstacleOrWall(new int[]{row - 2, col - 1}) &&
                	   exploredMap.getIsObstacleOrWall(new int[] {row - 2, col}) &&
                	   exploredMap.getIsObstacleOrWall(new int[] {row - 2, col + 1});
            case WEST:
                return exploredMap.getIsObstacleOrWall(new int[] {row + 1, col - 2}) &&
                	   exploredMap.getIsObstacleOrWall(new int[] {row, col - 2}) &&
                	   exploredMap.getIsObstacleOrWall(new int[] {row - 1, col - 2});
        }

        return false;
    }
    
    private DIRECTION getCalibrationDirection() {
        DIRECTION origDir = bot.getRobotDirection();
        DIRECTION dirToCheck;

        dirToCheck = DIRECTION.getNextDirection(origDir);                    // right turn
        if (CalibrateOnTheSpot(dirToCheck)) return dirToCheck;

        dirToCheck = DIRECTION.getPrevDirection(origDir);                // left turn
        if (CalibrateOnTheSpot(dirToCheck)) return dirToCheck;

        dirToCheck = DIRECTION.getPrevDirection(dirToCheck);             // u turn
        if (CalibrateOnTheSpot(dirToCheck)) return dirToCheck;

        return null;
    }
    
    private void calibrateBot(DIRECTION targetDir) {
        DIRECTION origDir = bot.getRobotDirection();

        turnBotDirection(targetDir);
        moveBot(MOVEMENT.CALIBRATION);
        turnBotDirection(origDir);
    }
    
    
	private void calibrateBotWhenReachedStart(robot bot)
	{
		System.out.println("Reached Start Calibrating now to reposition to north");
		switch(bot.getRobotDirection())
		{
		case WEST:
			bot.robotMovement(MOVEMENT.LEFT);
			bot.robotMovement(MOVEMENT.CALIBRATION);
			communicator.getCommunicator().recvMsg();
			bot.robotMovement(MOVEMENT.RIGHT);
			bot.robotMovement(MOVEMENT.CALIBRATION);
			communicator.getCommunicator().recvMsg();
			bot.robotMovement(MOVEMENT.RIGHT, false);
			break;
		case SOUTH:
			bot.robotMovement(MOVEMENT.CALIBRATION);
			communicator.getCommunicator().recvMsg();
			bot.robotMovement(MOVEMENT.RIGHT);
			bot.robotMovement(MOVEMENT.CALIBRATION);
			communicator.getCommunicator().recvMsg();
	        bot.robotMovement(MOVEMENT.RIGHT,false);
			break;
		default:
			break;
		}
		
		bot.setRobotDirection(DIRECTION.NORTH);
	} 
}
