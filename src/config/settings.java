package config;

public class settings {
	public static int COVERAGE = 300; // 100% exploration
	public static int TIME_LIMIT = 360000; // 6 minutes
	public static String ADDRESS = "192.168.19.19";
	public static int PORT = 5182;
	public static long CURRENT_TIMESTAMP = 0;
	public static long END_TIMESTAMP = 0;
	
}
