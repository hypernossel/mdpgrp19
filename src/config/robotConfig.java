package config;

public class robotConfig {
	// Properties for Robot
		public static final int GOAL_ROW = config.GOAL_LOC[0];                          // row no. of goal cell
	    public static final int GOAL_COL = config.GOAL_LOC[1];                          // col no. of goal cell
	    public static final int START_ROW = 1;                          // row no. of start cell
	    public static final int START_COL = 1;                          // col no. of start cell
	    public static final int MOVE_COST = 10;                         // cost of FORWARD, BACKWARD movement
	    public static final int TURN_COST = 20;                         // cost of RIGHT, LEFT movement
	    public static final int SPEED = 100;                            // delay between movements (ms)
	    public static final DIRECTION START_DIR = DIRECTION.NORTH;      // start direction
	    public static final int SENSOR_SHORT_RANGE_L = 1;               // range of short range sensor (cells)
	    public static final int SENSOR_SHORT_RANGE_H = 2;               // range of short range sensor (cells)
	    public static final int SENSOR_LONG_RANGE_L = 3;                // range of long range sensor (cells)
	    public static final int SENSOR_LONG_RANGE_H = 5;                // range of long range sensor (cells)
	    
	    
	    public enum DIRECTION {
	    	NORTH, EAST, SOUTH, WEST;
	    	
	    	//Get Next Direction
	    	public static DIRECTION getNextDirection(DIRECTION currDir)
	    	{
	    		return values()[(currDir.ordinal() + 1) % values().length];
	    	}
	    	
	    	public static DIRECTION getPrevDirection(DIRECTION currDir)
	    	{
	    		return values()[(currDir.ordinal() + values().length -1) % values().length];
	    	}
	    	
	    	public static char print(DIRECTION d) {
	            switch (d) {
	                case NORTH:
	                    return 'N';
	                case EAST:
	                    return 'E';
	                case SOUTH:
	                    return 'S';
	                case WEST:
	                    return 'W';
	                default:
	                    return 'X';
	            }
	        }
	    }
	    
	    public enum MOVEMENT
	    {
	    	FORWARD, BACKWARD,RIGHT,LEFT,CALIBRATION,BURST_2,BURST_3,ERROR;
	    	
	    	public static char print(MOVEMENT m) {
	            switch (m) {
	                case FORWARD:
	                    return 'w';
	                case BACKWARD:
	                    return 's';
	                case RIGHT:
	                    return 'd';
	                case LEFT:
	                    return 'a';
	                case CALIBRATION:
	                	return 'c';
	                case BURST_2:
	                	return 'q';
	                case BURST_3:
	                	return 'e';
	                case ERROR:
	                default:
	                    return 'x'; // ERROR
	            }
	        }
	    }
}
