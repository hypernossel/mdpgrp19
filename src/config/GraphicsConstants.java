package config;

import java.awt.*;

public class GraphicsConstants {
    public static final int CELL_LINE_WEIGHT = 2;

    public static final Color C_START = Color.BLUE;
    public static final Color C_GOAL = Color.RED;
    public static final Color C_UNEXPLORED = Color.GRAY;
    public static final Color C_FREE = Color.WHITE;
    public static final Color C_OBSTACLE = Color.BLACK;
    public static final Color C_VIRTUAL_WALL = Color.YELLOW;
    public static final Color C_WAYPOINT = Color.CYAN;
    
    public static final Color C_TRAIL = Color.ORANGE;

    public static final Color C_ROBOT = Color.BLACK;
    public static final Color C_ROBOT_DIR = Color.YELLOW;

    public static final int ROBOT_W = 70;
    public static final int ROBOT_H = 70;

    public static final int ROBOT_X_OFFSET = 10;
    public static final int ROBOT_Y_OFFSET = 20;

    public static final int ROBOT_DIR_W = 10;
    public static final int ROBOT_DIR_H = 10;

    public static final int CELL_SIZE = 30;

    public static final int MAP_H = 600;
    public static final int MAP_X_OFFSET = 120;
}