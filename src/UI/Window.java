package UI;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingWorker;
import javax.swing.WindowConstants;

import algo.AStar;
import algo.MapExploration;
import config.GraphicsConstants;
import config.robotConfig;
import config.robotConfig.DIRECTION;
import config.robotConfig.MOVEMENT;
import config.settings;
import map.Map;
import robot.robot;
import tools.communicator;
import tools.communicator.COMMANDS;
import tools.mapLoader;
import tools.utils;

public class Window {
	private static JFrame _appFrame = null;         // application JFrame

    private static JPanel _buttons = null;          // JPanel for buttons

    private static robot bot;
    private static Map realmap = null; 
    private static Map exploredMap = null;          // exploration map

    private static boolean realRun = false;
    
    private static JButton btn_Exploration,btn_FastestPath,btn_LoadMap,btn_EditMap,btn_Settings,btn_disconnect;
    
    public static JButton btn_Pause;
    
    private static JCheckBox cb_toggleFogOfWar;
    
    private static JTextField txt_MDF_1,txt_MDF_2;
    
    public static JLabel labelStatus;
    
    private static ArrayList<String> files = new ArrayList<String>();
    
    public static final communicator comms = communicator.getCommunicator();
    
    /**
     * Initialises the different maps and displays the application.
     */
    public static void main(String[] args) {

        bot = new robot(new int[] {robotConfig.START_ROW, robotConfig.START_COL}, realRun);

        if(!realRun)
        {
        	realmap = new Map(bot);
        	realmap.setAllUnexplored();
        }
        
        exploredMap = new Map(bot);
        exploredMap.setAllUnexplored();
        
        btn_Exploration = new JButton("Exploration");
        btn_FastestPath = new JButton("Fastest Path");
        btn_LoadMap = new JButton("Load Map");
        btn_disconnect = new JButton("Disconnect");
        cb_toggleFogOfWar = new JCheckBox("Fog of War");
        btn_EditMap = new JButton("Edit Map");
        btn_Settings = new JButton("Settings");
        cb_toggleFogOfWar.setSelected(true);
        labelStatus = new JLabel("Awating Connection......");
       
        btn_FastestPath.setEnabled(false);
        
        btn_Pause = new JButton("Pause");

        displayEverything();
    }
    
    
    
    /**
     * Initialises the different parts of the application.
     */
    private static void displayEverything() {
        // Initialise main frame for display
        _appFrame = new JFrame();
        _appFrame.setTitle("小 Tay Simulator ( MDP Group 19 )");
        _appFrame.setSize(new Dimension(1000, 700));
        _appFrame.setResizable(false);

        // Center the main frame in the middle of the screen
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        _appFrame.setLocation(dim.width / 2 - _appFrame.getSize().width / 2, dim.height / 2 - _appFrame.getSize().height / 2);

        // Create the JPanel for the buttons
        _buttons = new JPanel();

        // Add _mapCards & _buttons to the main frame's content pane
        Container contentPane = _appFrame.getContentPane();
        contentPane.add(_buttons, BorderLayout.PAGE_END);

        displaygrid();
        initButtonsLayout();
        // Initialize the main map view
        initMainLayout();

        // Display the application
        _appFrame.setVisible(true);
        _appFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }
    
    /**
     * Initialises the main map view by adding the different maps as cards in the CardLayout. Displays realMap
     * by default.
     */
    private static void initMainLayout() {        
        exploredMap.setOpaque(false);
        _appFrame.getContentPane().add(exploredMap);  
    }
    
    private static void displaygrid()
    {
    	for(int row=19; row>=0; row--)
    	{
    		if(row < 15) {
	    		JLabel col_lbl = new JLabel(row + "");
				col_lbl.setHorizontalAlignment(SwingConstants.CENTER);
				col_lbl.setVerticalAlignment(SwingConstants.CENTER);
				col_lbl.setBounds((row * (GraphicsConstants.CELL_SIZE)) + row + 120, 0, 20, 20);
				col_lbl.setHorizontalAlignment(SwingConstants.CENTER);
				col_lbl.setVerticalAlignment(SwingConstants.CENTER);
				col_lbl.setVisible(true);
				col_lbl.setOpaque(false);
				_appFrame.add(col_lbl);
    		}
    		
    		JLabel lbln = new JLabel(row + "");
			lbln.setHorizontalAlignment(SwingConstants.CENTER);
			lbln.setVerticalAlignment(SwingConstants.TOP);
			lbln.setBounds(80, _appFrame.getHeight() - 100 - (row * (GraphicsConstants.CELL_SIZE-1)) - row, 20, 20);
			lbln.setHorizontalAlignment(SwingConstants.CENTER);
			lbln.setVerticalAlignment(SwingConstants.CENTER);
			lbln.setVisible(true);
			lbln.setOpaque(false);
			_appFrame.add(lbln);
    	}
    	
    }

    /**
     * Initialises the JPanel for the buttons.
     */
    private static void initButtonsLayout() {
        _buttons.setLayout(new GridLayout());
        addButtons();
    }

    /**
     * Helper method to set particular properties for all the JButtons.
     */
    private static void formatButton(JButton btn) {
        btn.setFont(new Font("Arial", Font.BOLD, 13));
        btn.setFocusPainted(false);
    }
    
    /**
     * Initialises and adds the five main buttons. Also creates the relevant classes (for multithreading) and JDialogs
     * (for user input) for the different functions of the buttons.
     */
    private static void addButtons() {
    	
    	class MoveFastest extends SwingWorker<Integer, String>{

			@Override
			protected Integer doInBackground() throws Exception {
				settings.END_TIMESTAMP += settings.TIME_LIMIT;
				//if(communicator.getCommunicator().recvMsg().equals("FP_START"));
				
				AStar.moveRobot(exploredMap,realmap,bot, exploredMap.getCell(new int[] {1,1}).getID(), exploredMap.getCell(config.config.GOAL_LOC).getID(), false,true);
				return 333;
			}
			
			@Override
            protected void done() {
            	
				btn_FastestPath.setText("Fastest Path");
                btn_Exploration.setEnabled(true);
                
                labelStatus.setText("Fastest Path Done");
                
                btn_FastestPath.setEnabled(false);
                btn_Exploration.setEnabled(true);
            };
        }
        
     // Exploration Class for Multithreading
    	
        class Exploration extends SwingWorker<Integer, String> {
        	MapExploration exploration = null;
            protected Integer doInBackground() throws Exception {            	
                int row, col;

                row = robotConfig.START_ROW;
                col = robotConfig.START_COL;
                int[] coord = {row,col};
                bot.setPos(coord);
                exploredMap.repaint();
                exploration = new MapExploration(exploredMap,realmap, bot, settings.COVERAGE);
                exploration.run();
                
                return 111;
            }
            @Override
            protected void done() {
            	
            	txt_MDF_1.setText(utils.generateMDF(exploredMap));
            	txt_MDF_2.setText(utils.generateMDF2(exploredMap));
            	
            	btn_Exploration.setText("Exploration");
                btn_Exploration.setEnabled(true);
                
                labelStatus.setText(exploration.getOutputString());
                
                cb_toggleFogOfWar.setEnabled(true);
                btn_FastestPath.setEnabled(true);
                btn_Exploration.setEnabled(false);
            };
        }
        
        // Exploration Button
        
        btn_Exploration.setBounds(_appFrame.getWidth()/2 + 150, _appFrame.getHeight()/2 + 80, 200, 50);
        formatButton(btn_Exploration);
        btn_Exploration.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
            	if(btn_Exploration.isEnabled()) {
            		int response = JOptionPane.showConfirmDialog(_appFrame,"Is it Real Run?", "MDP GRP 19 - Exploration",JOptionPane.YES_NO_OPTION);
            		if(response == JOptionPane.YES_OPTION) {
            			comms.openConnection(settings.ADDRESS, settings.PORT);
            			if(comms.isConnected())
            				btn_disconnect.setText("Disconnect");
            			
            			bot.setRealRun(true);
            		}

            		btn_Exploration.setText("Exploring...");
            		labelStatus.setText("Exploring....");
            		new Exploration().execute();
            	}
            	btn_Exploration.setEnabled(false);
            	cb_toggleFogOfWar.setEnabled(false);
            }
        });
        
        _appFrame.getContentPane().add(btn_Exploration);

        // Fastest Path Button
        btn_FastestPath.setBounds(_appFrame.getWidth()/2 + 150, _appFrame.getHeight()/2 + 2 * 70, 200, 50);
        formatButton(btn_FastestPath);
        btn_FastestPath.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
            	if(btn_FastestPath.isEnabled()) {
            		if(!comms.isConnected())
            		{
            			int response = JOptionPane.showConfirmDialog(_appFrame,"Is it Real Run?", "MDP GRP 19 - Fastest Path",JOptionPane.YES_NO_OPTION);
                		if(response == JOptionPane.YES_OPTION) {
                			
                			if(!comms.isConnected()) {
                				comms.openConnection(settings.ADDRESS, settings.PORT);
                			}
                			btn_disconnect.setText("Disconnect");
                			bot.setRealRun(true);
                		}
            		}
            		
            		btn_FastestPath.setText("Running Fastest Path...");
            		labelStatus.setText("Running Fastest Path....");
            		new MoveFastest().execute();
            	}
            	
            }
        });
        _appFrame.getContentPane().add(btn_FastestPath);
        
        // Load Map
        btn_LoadMap.setBounds(_appFrame.getWidth()/2 + 150, _appFrame.getHeight()/2 + 3 * 65, 200, 50);
        btn_LoadMap.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                JDialog loadMapDialog = new JDialog(_appFrame, "Load Map", true);
                loadMapDialog.setSize(400, 60);
                loadMapDialog.setLayout(new FlowLayout());
                
                listFilesForFolder(new File("samplemap/"));
                
                String[] _files = Arrays.copyOf(files.toArray(), files.size(), String[].class);

                final JComboBox loadTF = new JComboBox(_files);
                JButton loadMapButton = new JButton("Load");

                loadMapButton.addMouseListener(new MouseAdapter() {
                    public void mousePressed(MouseEvent e) {
                        loadMapDialog.setVisible(false);
                        bot.setPos(new int[] {robotConfig.START_ROW,robotConfig.START_COL});
                        mapLoader.loadMapFromDisk(realmap, _files[loadTF.getSelectedIndex()]);
                        mapLoader.loadMapFromDisk(exploredMap, _files[loadTF.getSelectedIndex()]);
                        
                        System.out.println(_files[loadTF.getSelectedIndex()]);
                        JOptionPane.showMessageDialog(_appFrame,
            				    "Map has been loaded.",
            				    "MDP Group 19",
            				    JOptionPane.INFORMATION_MESSAGE);
                    }
                });

                loadMapDialog.add(new JLabel("File Name: "));
                loadMapDialog.add(loadTF);
                loadMapDialog.add(loadMapButton);
                loadMapDialog.setLocationRelativeTo(null);
                loadMapDialog.setVisible(true);
            }
        });
        _appFrame.getContentPane().add(btn_LoadMap);
        
        JLabel lblOptions = new JLabel("Options");
        lblOptions.setBounds(_appFrame.getWidth()/2 + 150, _appFrame.getHeight()/2 - 80,100,50);
        cb_toggleFogOfWar.setBounds(_appFrame.getWidth()/2 + 150, _appFrame.getHeight()/2 - 50,100,50);
        cb_toggleFogOfWar.addMouseListener(new MouseAdapter() {
        	public void mousePressed(MouseEvent e)
        	{
        		if(cb_toggleFogOfWar.isEnabled())
        			exploredMap.hideFog();
        	}
		});
        _appFrame.getContentPane().add(lblOptions);
        _appFrame.getContentPane().add(cb_toggleFogOfWar);
        JLabel lblMDFString_1 = new JLabel("MDF_1");
        lblMDFString_1.setBounds(_appFrame.getWidth()/2 + 150, _appFrame.getHeight()/2 - 3*70,100,50);
        txt_MDF_1 = new JTextField();
        txt_MDF_1.setBounds(_appFrame.getWidth()/2 + 150, _appFrame.getHeight()/2 - 3*70 + 40,300,30);
        _appFrame.getContentPane().add(txt_MDF_1);
        _appFrame.getContentPane().add(lblMDFString_1);
        
        JLabel lblMDFString_2 = new JLabel("MDF_2");
        lblMDFString_2.setBounds(_appFrame.getWidth()/2 + 150, _appFrame.getHeight()/2 - 2*70,100,50);
        txt_MDF_2 = new JTextField();
        txt_MDF_2.setBounds(_appFrame.getWidth()/2 + 150, _appFrame.getHeight()/2 - 2*70 + 40,300,30);
        _appFrame.getContentPane().add(txt_MDF_2);
        _appFrame.getContentPane().add(lblMDFString_2);
        
        labelStatus.setBounds(_appFrame.getWidth()/2 + 200, 0, 300, 150);
        _appFrame.getContentPane().add(labelStatus);
        
        btn_Settings.setBounds(_appFrame.getWidth()/2 + 250, _appFrame.getHeight()/2,100,50);
        btn_Settings.addMouseListener(new MouseAdapter() {
        	public void mousePressed(MouseEvent e)
        	{
        		 JDialog settingDialog = new JDialog(_appFrame, "Settings", true);
        		 settingDialog.setSize(200, 250);
                     
                 JPanel jPanel = new JPanel();
                 jPanel.setLayout(new GridBagLayout());
                 GridBagConstraints c = new GridBagConstraints();
                 c.fill = GridBagConstraints.BOTH;
                 c.gridx = 0;
                 c.gridheight = 1;
                 c.gridwidth = 2;
                 
                 JLabel lblCoverage = new JLabel("Coverage Limit ( Cells )");
                 lblCoverage.setBounds(0,0,100,50);
                 c.gridy = 0;
                 jPanel.add(lblCoverage,c);
                 
                 JTextField tfCoverage = new JTextField();
                 tfCoverage.setBounds(0,0,200,50);
                 tfCoverage.setText(Integer.toString(settings.COVERAGE));
                 c.gridy = 2;
                 jPanel.add(tfCoverage,c);
                 
                 JLabel lblTimeLimit = new JLabel("Time ( Min : Sec )");
                 c.gridy = 4;
                 c.gridwidth = 2;
                 jPanel.add(lblTimeLimit,c);
                 
                 JComboBox<Integer> cb_minute = new JComboBox<Integer>(); 
                 JComboBox<Integer> cb_seconds = new JComboBox<Integer>();
                 
                 for(int i=0; i<=60; i++) {
                	 cb_minute.addItem(i);
                	 cb_seconds.addItem(i);
                 }
                 c.gridx = 0;
                 c.gridy = 6;
                 c.gridwidth = 1;
                 cb_minute.setSelectedIndex(1);
                 jPanel.add(cb_minute,c);
                 
                 c.gridx = 1;
                 c.gridy = 6;
                 c.gridwidth = 1;
                 jPanel.add(cb_seconds,c);
                 
                 c.gridx = 0;
                 c.gridwidth = 2;
                 
                 JLabel lblRPI = new JLabel("Rpi Address");
                 c.gridy = 8;
                 jPanel.add(lblRPI,c);
                 
                 JTextField tfRPIAddress = new JTextField();
                 tfRPIAddress.setText(settings.ADDRESS);
                 c.gridy = 10;
                 jPanel.add(tfRPIAddress,c);
                 
                 JLabel lblRPI_PORT = new JLabel("Rpi Port");
                 c.gridy = 12;
                 jPanel.add(lblRPI_PORT,c);
                 
                 JTextField tfRPIPORT = new JTextField();
                 tfRPIPORT.setText(Integer.toString(settings.PORT));
                 c.gridy = 14;
                 jPanel.add(tfRPIPORT,c);
                 
                 JButton btn_TestConn = new JButton("Test Command");
                 class TestConnection extends SwingWorker<Integer, String> {
                     protected Integer doInBackground() throws Exception {
                    	 comms.openConnection(settings.ADDRESS, settings.PORT);
                    	 comms.sendMsg("awwwwwdwwwwwdwdwawwwwt", COMMANDS.INSTRUCTIONS,false);
                    	 return 555;
                     }
                     @Override
                     protected void done() {
                    	 if(comms != null)
                    		 if(comms.isConnected())
                    			 btn_disconnect.setText("Disconnect");
                    		 else
                    			 labelStatus.setText("Awaiting Connection");
                    	 else
                    		 labelStatus.setText("Communicator is NULL and Failed miserably.");
                    	 btn_TestConn.setEnabled(true);
                     };
                 }
               
                 c.gridy = 16;
                 btn_TestConn.addMouseListener(new MouseAdapter() {
                	 public void mousePressed(MouseEvent e)
                	 {
                		 if(btn_TestConn.isEnabled())
                		 {
	                		 btn_TestConn.setEnabled(false);
	                		 labelStatus.setText("Establishing Connection.... Please Wait");
	                		 new TestConnection().execute();
                		 } 
                	 }
                 });
                 
                 jPanel.add(btn_TestConn,c);
                 
                 JButton btn_SaveSettings = new JButton("Save Settings");
                 c.gridy = 18;
                 btn_SaveSettings.addMouseListener(new MouseAdapter() {
                	 public void mousePressed(MouseEvent e)
                	 {
                		 try
                		 {
                			 settings.COVERAGE = Integer.parseInt(tfCoverage.getText());
                    		 settings.TIME_LIMIT = ((int)cb_minute.getSelectedItem() * 60 * 1000) + ((int)cb_seconds.getSelectedItem() * 1000);
                    		 settings.ADDRESS = (tfRPIAddress.getText() != "") ? tfRPIAddress.getText() : settings.ADDRESS;
                    		 settings.PORT = Integer.parseInt(tfRPIPORT.getText());
                    		 JOptionPane.showMessageDialog(_appFrame,
                    				    "Settings have been saved.",
                    				    "Settings",
                    				    JOptionPane.INFORMATION_MESSAGE);
                    		 settingDialog.setVisible(false);
                		 }catch (NullPointerException ne) {
							// TODO: handle exception
                			 System.out.println("Please do not leave any fields blank thank you.");
                		 }catch (NumberFormatException nfe)
                		 {
                			 System.out.println("Parsing Error. Please check if Coverage is in Int,TimeLimit is in int,Port is in Int");
                		 }
                	 }
                 });
                 
                 jPanel.add(btn_SaveSettings,c);
                 settingDialog.getContentPane().add(jPanel);
                 settingDialog.setLocationRelativeTo(null);
                 settingDialog.setVisible(true);
        	}
        });
        _appFrame.getContentPane().add(btn_Settings);
        
        btn_disconnect.setBounds(_appFrame.getWidth()/2 + 350, _appFrame.getHeight()/2, 100, 50);
        btn_disconnect.setText("Awaiting Connection....");
        btn_disconnect.addMouseListener(new MouseAdapter() {
        	public void mousePressed(MouseEvent e)
        	{
        		if(btn_disconnect.getText().toLowerCase().equals("disconnect")) {
        			comms.closeConnection();
        			btn_disconnect.setText("Awaiting Connection...");
        		}
        	}
        });
        _appFrame.getContentPane().add(btn_disconnect);
        
        btn_EditMap.setBounds(_appFrame.getWidth()/2 + 150, _appFrame.getHeight()/2,100,50);
        btn_EditMap.addMouseListener(new MouseAdapter() {
        	public void mousePressed(MouseEvent e)
        	{
        		 JDialog editMapDialog = new JDialog(_appFrame, "Edit Arena", true);
        		 editMapDialog.setSize(400, 650);
                 Map arena = exploredMap;
                 
                 exploredMap.hideFog();
                 
                 JPanel jArena = new JPanel();
                 jArena.setLayout(new GridBagLayout());
                 GridBagConstraints c = new GridBagConstraints();
                 c.fill = GridBagConstraints.VERTICAL;
                 
                 for(int row=0; row<20; row++)
                 {
                	 for(int col=0; col< 15; col++)
                	 {
                		 JButton btn_Cell = new JButton();
                		 btn_Cell.setToolTipText(row + ":" + col);
                		 btn_Cell.setBounds(10 + (col * 20) + (col * 5),20 + (((row * 20)) +  (row * 5)), 20, 20);
                		 
                		 btn_Cell.setName(row + ":" + col);
                		 
                		 //Setting Map Legends
                		 if(arena.inStartZone(row, col)) {
                			 btn_Cell.setBackground(GraphicsConstants.C_START);
                			 btn_Cell.setEnabled(false);
                		 }else if(arena.inGoalZone(row, col)) {
                			 btn_Cell.setBackground(GraphicsConstants.C_GOAL);
                		 	 btn_Cell.setEnabled(false);
                		 }else
                		 {
                			 if (arena.getCell(new int[] {row,col}).isObsticle())
                            	 btn_Cell.setBackground(GraphicsConstants.C_OBSTACLE);
                             else if(arena.getCell(new int[] {row,col}).isVirtualWall())
                            	 btn_Cell.setBackground(GraphicsConstants.C_VIRTUAL_WALL);
                             else
                            	 btn_Cell.setBackground(GraphicsConstants.C_FREE);
                		 }
                		 btn_Cell.setMaximumSize(new Dimension(5, 5));
                		 btn_Cell.setBorderPainted(false);
                		 btn_Cell.setOpaque(true);
                		 btn_Cell.addMouseListener(new MouseAdapter() {
                	            public void mousePressed(MouseEvent e) {
                	            	if(btn_Cell.isEnabled()) {
                	            		String CellCoord[] = btn_Cell.getToolTipText().split(":");
                	            		int row = Integer.parseInt(CellCoord[0]);
            	            			int col = Integer.parseInt(CellCoord[1]);
            	            			
                	            		int id = e.getClickCount() % 2;
                	            		System.out.println(id);
                	            		switch(id)
                	            		{
	                	            		case 0:
	                	            			if(btn_Cell.getBackground() == GraphicsConstants.C_FREE ||
	                	            			   btn_Cell.getBackground() == GraphicsConstants.C_WAYPOINT ||
	                	            			   btn_Cell.getBackground() == GraphicsConstants.C_VIRTUAL_WALL)
	                    	            		{
	                    	            			//if((row > 0 && row < 19) && (col > 0 && col < 14)) {
	                    	            				btn_Cell.setBackground(GraphicsConstants.C_OBSTACLE);
	                    	            				arena.setObstacleCell(Integer.parseInt(CellCoord[0]),
	                    	            										Integer.parseInt(CellCoord[1]), true);
	                    	            			//}
	                    	            		}else
	                    	            		{
	                    	            			btn_Cell.setBackground(GraphicsConstants.C_FREE);
	                    	            			arena.setObstacleCell(Integer.parseInt(CellCoord[0]),
	                										Integer.parseInt(CellCoord[1]), false);
	                    	            			
	                    	            		}
	                	            			break;
	                	            		case 1:
	                	            			btn_Cell.setBackground(GraphicsConstants.C_WAYPOINT);
	                	            			arena.setWayPoint(row, col, true);
	                	            			break;
                	            		}
                	            		updateButtons(jArena, arena);
                	            		jArena.repaint();
                	            	}
                	            }
                	        });
                		 c.weightx = 1.0;
                		 c.weighty = 1.0;
                		 c.gridx = col;
                		 c.gridy = (20-row);
                		 c.gridwidth = 1;
                		 c.gridheight = 1;
                		 c.insets = new Insets(2,2,2,2);
                		 jArena.add(btn_Cell,c);

                	 }
                 }
                 
                 c.gridx = 0;
                 c.gridy = 21;
                 c.gridwidth = 6;
                 c.gridheight = 1;
                 c.insets = new Insets(2,2,2,2);
                 JButton SaveMap = new JButton("Save Map");
                 
                 
                 SaveMap.addMouseListener(new MouseAdapter() {
	                	 public void mousePressed(MouseEvent e) {
	                		exploredMap.setAllUnexplored();
	     	            	exploredMap.repaint();
	     	            	JOptionPane.showMessageDialog(_appFrame,
                				    "Map have been updated.",
                				    "Map Editor",
                				    JOptionPane.INFORMATION_MESSAGE);
	     	            	exploredMap.hideFog();
	     	            	editMapDialog.dispose();
	     	            }
					});
                 jArena.add(SaveMap,c);
                 
                 c.gridx = 7;
                 c.gridy = 21;
                 c.gridwidth = 6;
                 c.gridheight = 1;
                 c.insets = new Insets(2,2,2,2);
                 JButton ClearMap = new JButton("Clear Map");
                 
                 ClearMap.addMouseListener(new MouseAdapter() {
                	 public void mousePressed(MouseEvent e)
                	 {
                		 arena.clearMap();
                		 updateButtons(jArena, arena);
                		 jArena.repaint();
                		 JOptionPane.showMessageDialog(_appFrame,
             				    "Map has been cleared.",
             				    "Map Editor",
             				    JOptionPane.INFORMATION_MESSAGE);
                	 }
                 });
                 
                 jArena.add(ClearMap,c);
                 
                 editMapDialog.getContentPane().add(jArena);
                 editMapDialog.setLocationRelativeTo(null);
                 editMapDialog.setVisible(true);
                    
        	}
		});
        
        _appFrame.getContentPane().add(btn_EditMap);
        
        
    }
    public static void resetExplorationBtn() {
    	btn_Exploration.setEnabled(true);
    }
    
    public static void updateButtons(JPanel jarena,Map arena)
    {
    	for(int row=0; row<20; row++)
        {
	       	 for(int col=0; col< 15; col++)
	       	 {
	       		Component[] comp = jarena.getComponents();
	       	    for (int i = 0;i<comp.length;i++) {
	       	        if (comp[i] instanceof JButton) {
	       	        	JButton btn_Cell = (JButton)comp[i];
	       	        	if(btn_Cell.getName().equals(row + ":" + col)) {
		       	        	if(arena.inStartZone(row, col)) {
			   	       			 btn_Cell.setBackground(GraphicsConstants.C_START);
			   	       			 btn_Cell.setEnabled(false);
			   	       		 }else if(arena.inGoalZone(row, col)) {
			   	       			 btn_Cell.setBackground(GraphicsConstants.C_GOAL);
			   	       		 	 btn_Cell.setEnabled(false);
			   	       		 }else
			   	       		 {
			   	       			 if (arena.getCell(new int[] {row,col}).isObsticle())
			   	                   	 btn_Cell.setBackground(GraphicsConstants.C_OBSTACLE);
			   	                    else if(arena.getCell(new int[] {row,col}).isVirtualWall())
			   	                   	 btn_Cell.setBackground(GraphicsConstants.C_VIRTUAL_WALL);
			   	                    else if(arena.getCell(new int[] {row,col}).isWayPoint())
			   	                     btn_Cell.setBackground(GraphicsConstants.C_WAYPOINT);
			   	                    else if(arena.getCell(new int[] {row,col}).isTrail())
			   	                     btn_Cell.setBackground(GraphicsConstants.C_TRAIL);
			   	                    else
			   	                   	 btn_Cell.setBackground(GraphicsConstants.C_FREE);
			   	       		 }
		       	        	break;
	       	        	}
	       	        }
	       	    }
	       		
	       	 }
       	}
    }
    
    public static void listFilesForFolder(final File folder) {
	    for (final File fileEntry : folder.listFiles()) {
	        if (fileEntry.isDirectory()) {
	            listFilesForFolder(fileEntry);
	        } else {
	        	files.add(fileEntry.getName());
	        }
	    }
	}
}
