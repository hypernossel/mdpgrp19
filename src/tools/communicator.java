package tools;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;

import UI.Window;

public class communicator {
	
	private static communicator Mgr = null;
    private static Socket conn = null;

    private BufferedWriter writer;
    private BufferedReader reader;
    
	public enum COMMANDS{
		EX_START,
		FP_START,
		MAP,
		F_BOT_POS,
		BOT_START,
		BOT_END,
		INSTRUCTIONS,
		SENSOR_DAT;
		
		public static String getCommandString(COMMANDS type)
		{
			switch(type)
			{
			case EX_START:
				return "EX_START";
			case FP_START:
				return "FP_START";
			case MAP:
				return "MAP";
			case F_BOT_POS:
				return "F_BOT_POS";
			case BOT_START:
				return "BOT_START";
			case BOT_END:
				return "BOT_END";
			case INSTRUCTIONS:
				return "INSTRUCTION";
			case SENSOR_DAT:
				return "SENSOR";			
			}
			return null;
		}
	}
	
	private communicator() {}
	
	public static communicator getCommunicator() {
		if(Mgr == null)
			Mgr = new communicator();
		return Mgr;
	}
	
	public void openConnection(String address,int port) {
        System.out.println("Opening connection...");

        try {
            conn = new Socket(address, port);
            writer = new BufferedWriter(new OutputStreamWriter(new BufferedOutputStream(conn.getOutputStream())));
            reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            
            System.out.println("openConnection() --> " + "Connection established successfully!");
            return;
        } catch (UnknownHostException e) {
            System.out.println("openConnection() --> UnknownHostException");
        } catch (IOException e) {
            System.out.println("openConnection() --> IOException");
        } catch (Exception e) {
            System.out.println("openConnection() --> Exception");
            System.out.println(e.toString());
        }

        System.out.println("Failed to establish connection!");
    }
	
	public void closeConnection() {
        System.out.println("Closing connection...");

        try {
            reader.close();

            if (conn != null) {
                conn.close();
                conn = null;
            }
            System.out.println("Connection closed!");
        } catch (IOException e) {
            System.out.println("closeConnection() --> IOException");
        } catch (NullPointerException e) {
            System.out.println("closeConnection() --> NullPointerException");
        } catch (Exception e) {
            System.out.println("closeConnection() --> Exception");
            System.out.println(e.toString());
        }
    }
	
	public void sendMsg(String msg, COMMANDS msgType,boolean isAndroid) {
        System.out.println("Sending a message...");
        String affix = (isAndroid) ? "a_" : "h_";
        try {
            String outputMsg;
            if (msg == null) {
                outputMsg = affix + COMMANDS.getCommandString(msgType);
            } else if (msgType == COMMANDS.MAP) {
                outputMsg = affix + COMMANDS.getCommandString(msgType) + ":" + msg + " " ;
            } else if(msgType == COMMANDS.INSTRUCTIONS) {
            	outputMsg = affix + msg;
            }else
            	outputMsg = affix + COMMANDS.getCommandString(msgType) + ":" + msg;
          
            System.out.println("Sending out message:\n" + outputMsg);
            writer.write(outputMsg);
            writer.flush();
            System.out.println("data sent.");
        } catch (IOException e) {
            System.out.println("sendMsg() --> IOException");
        } catch (Exception e) {
            System.out.println("sendMsg() --> Exception");
            System.out.println(e.toString());
        }
    }
	
	public String recvMsg() {
        System.out.println("Receiving a message...");
        try {
        	while(true)
        	{
        		String input = reader.readLine();
        		if (input != null && input.length() > 0) {
        			System.out.println("communicator: " + input);
                    return input;
                }
        	}
        } catch (IOException e) {
        	System.out.println(e.getMessage());
            System.out.println("recvMsg() --> IOException");
        } catch (Exception e) {
            System.out.println("recvMsg() --> Exception");
            System.out.println(e.toString());
        }

        return null;
    }
	
	public boolean isConnected() {
		if(conn != null)
			return conn.isConnected();
		return false;
    }
}
