package tools;

import map.Cell;
import map.Map;
import config.config;

public class utils {
	
	public static String Bin2Hex(String bin)
	{
		int dec = Integer.parseInt(bin,2);
		//System.out.println("BIN: " + bin + ", " + "Dec: " + dec +", HEX:" + Integer.toString(dec,16));
		return Integer.toString(dec,16);
	}
	
	/*public static String[] generateMapDescriptor(Map map) {
        String[] ret = new String[2];

        StringBuilder Part1 = new StringBuilder();
        StringBuilder Part1_bin = new StringBuilder();
        Part1_bin.append("11");
        for (int r = config.GRID_ROW-1; r > 0; r--) {
            for (int c =0; c < config.GRID_COL; c++) {
                if (map.getCell(new int[] {r,c}).isVisited())
                    Part1_bin.append("1");
                else
                    Part1_bin.append("0");

                if (Part1_bin.length() == 4) {
                    Part1.append(Bin2Hex(Part1_bin.toString()));
                    Part1_bin.setLength(0);
                }
            }
            System.out.println("");
        }
        Part1_bin.append("11");
        Part1.append(Bin2Hex(Part1_bin.toString()));
        System.out.println("P1: " + Part1.toString());
        ret[0] = Part1.toString();

        StringBuilder Part2 = new StringBuilder();
        StringBuilder Part2_bin = new StringBuilder();
        for (int r = config.GRID_ROW-1; r >0 ; r--) {
            for (int c = 0; c < config.GRID_COL; c++) {
                if (map.getCell(new int[] {r,c}).isVisited()) {
                    if (map.getCell(new int[] {r,c}).isVisited())
                        Part2_bin.append("1");
                    else
                        Part2_bin.append("0");

                    if (Part2_bin.length() == 4) {
                        Part2.append(Bin2Hex(Part2_bin.toString()));
                        Part2_bin.setLength(0);
                    }
                }
            }
        }
        if (Part2_bin.length() > 0) Part2.append(Bin2Hex(Part2_bin.toString()));
        System.out.println("P2: " + Part2.toString());
        ret[1] = Part2.toString();

        return ret;
    }*/
	
	public static String RawMDP(Map map)
	{
		int[] pos = {0,0};
		int count = 2;
		int[] track = new int[300];
		String bin = "11";
		String ret = "";
		
		for(int i = 0; i < 20; i ++) {
			pos[0] = i;
			for(int j = 0; j < 15; j++) {
				pos[1] = j;
				Cell c = map.getCell(pos);
				if(c.isVisited()) {
					bin = bin + "1";
					track[i*15+j] = 1;
				}
				else {
					bin = bin + "0";
				}
				count += 1;
				if(count == 4) {
					ret = ret + bin;
					count = 0;
					bin = "";
				}
			}
		}
		return ret + bin + "11";
	}
	
	public static String generateMDF(Map map) {
		int[] pos = {0,0};
		int count = 2;
		int[] track = new int[300];
		String bin = "11";
		String ret = "";
		for(int i = 0; i < 20; i ++) {
			pos[0] = i;
			for(int j = 0; j < 15; j++) {
				pos[1] = j;
				Cell c = map.getCell(pos);
				if(c.isVisited()) {
					bin = bin + "1";
					track[i*15+j] = 1;
				}
				else {
					bin = bin + "0";
				}
				count += 1;
				if(count == 4) {
					ret = ret + Bin2Hex(bin);
					count = 0;
					bin = "";
				}
			}
		}
		return ret + Bin2Hex(bin + "11");
	}
	
	public static String generateMDF2(Map map) {
		
	    int[] pos = {0,0};
	    int count = 0;
	    String bin = "";
	    String ret = "";
	    for(int i = 0; i <= 19; i++) {
	      pos[0] = i;
	      for(int j = 0; j < 15; j++) {
	        pos[1] = j;
	        Cell c = map.getCell(pos);
	        //System.out.println(pos[0] + ", " + pos[1] + "  - " + c.isObsticle());
	        if(c.isVisited()) {
		        if(c.isObsticle() && c.isVirtualWall() || c.isObsticle()) {
		          bin = bin + "1";
		        }
		        else
		        {
		          bin = bin + "0";
		        }
		        if(count == 3) {
		          ret = ret + Bin2Hex(bin);
		          bin = "";
		          count = 0;
		        }
		        else {
		          count += 1;
		        }
	        }
	      }
	    }
	    return ret;
	 }
}
