package tools;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import map.Map;
import config.config;;

public class mapLoader {
	public static void loadMapFromDisk(Map map, String filename) {
        try {
            InputStream inputStream = new FileInputStream("samplemap/" + filename);
            BufferedReader buf = new BufferedReader(new InputStreamReader(inputStream));
            
            //Clear Map
            map.clearMap();

            String line = buf.readLine();
            StringBuilder sb = new StringBuilder();
            while (line != null) {
                sb.append(line);
                line = buf.readLine();
            }

            String bin = sb.toString();
            int binPtr = 0;
            for (int row = config.GRID_ROW - 1; row >= 0; row--) {
                for (int col = 0; col < config.GRID_COL; col++) {
                    if (bin.charAt(binPtr) == '1') map.setObstacleCell(row, col, true);
                    binPtr++;
                }
            }

            map.setAllUnexplored();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
