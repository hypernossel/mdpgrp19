package robot;

import config.robotConfig.DIRECTION;
import map.Map;

public class sensor {
	private final int lowerRange;
    private final int upperRange;
    private int sensorPosRow;
    private int sensorPosCol;
    private DIRECTION sensorDir;
    private final String id;
    
    public sensor(int lowerRange, int upperRange, int row, int col, DIRECTION dir, String id) {
        this.lowerRange = lowerRange;
        this.upperRange = upperRange;
        this.sensorPosRow = row;
        this.sensorPosCol = col;
        this.sensorDir = dir;
        this.id = id;
    }
    
    public void setSensor(int row, int col, DIRECTION dir) {
        this.sensorPosRow = row;
        this.sensorPosCol = col;
        this.sensorDir = dir;
    }
    
    public int sense(Map exploredMap,Map realMap) {
        switch (sensorDir) {
            case NORTH:
                return getSensorVal(exploredMap,realMap, new int[] {1, 0});
            case EAST:
                return getSensorVal(exploredMap,realMap, new int[] {0, 1});
            case SOUTH:
                return getSensorVal(exploredMap,realMap, new int[] {-1, 0});
            case WEST:
                return getSensorVal(exploredMap,realMap, new int[] {0, -1});
        }
        return -1;
    }
    
    private int getSensorVal(Map exploredMap,Map realMap, int[] positionInc) {
    	
        // Check if starting point is valid for sensors with lowerRange > 1.
        if (lowerRange > 1) {
            for (int i = 1; i < this.lowerRange; i++) {
                int row = this.sensorPosRow + (positionInc[0] * i);
                int col = this.sensorPosCol + (positionInc[1] * i);

                if (!exploredMap.checkValidCoordinates(new int[] {row,col})) return i;
                if (realMap.getCell(new int[] {row,col}).isObsticle()) return i;
            }
        }

        // Check if anything is detected by the sensor and return that value.
        for (int i = this.lowerRange; i <= this.upperRange; i++) {
            int row = this.sensorPosRow + (positionInc[0] * i);
            int col = this.sensorPosCol + (positionInc[1] * i);

            if (!exploredMap.checkValidCoordinates(new int[] {row,col})) return i;
            
            exploredMap.getCell(new int[] {row,col}).setVisited(true); 
            
            if (realMap.getCell(new int[] {row,col}).isObsticle()) {
                exploredMap.setObstacleCell(row, col, true);
                return i;
            }
            
        }

        // Else, return -1.
        return -1;
    }
    
    /**
     * Uses the sensor direction and given value from the actual sensor to update the map.
     */
    public void senseReal(Map exploredMap, int sensorVal) {
        switch (sensorDir) {
            case NORTH:
                processSensorVal(exploredMap, sensorVal, new int[]{1, 0});
                break;
            case EAST:
                processSensorVal(exploredMap, sensorVal, new int[] {0, 1});
                break;
            case SOUTH:
                processSensorVal(exploredMap, sensorVal, new int[] {-1, 0});
                break;
            case WEST:
                processSensorVal(exploredMap, sensorVal, new int[] {0, -1});
                break;
        }
    }
    
    /**
     * Sets the correct cells to explored and/or obstacle according to the actual sensor value.
     */
    private void processSensorVal(Map exploredMap, int sensorVal, int[] positionInc) {
    	if (sensorVal == 0) return;  // return value for LR sensor if obstacle before lowerRange

        // If above fails, check if starting point is valid for sensors with lowerRange > 1.
        for (int i = 1; i < this.lowerRange; i++) {
            int row = this.sensorPosRow + (positionInc[0] * i);
            int col = this.sensorPosCol + (positionInc[1] * i);

            if (!exploredMap.checkValidCoordinates(new int[] {row,col})) return;
            if (exploredMap.getCell(new int[] {row,col}).isObsticle()) return;
        }

        // Update map according to sensor's value.
        for (int i = this.lowerRange; i <= this.upperRange; i++) {
        	
        	int row = this.sensorPosRow + (positionInc[0] * i);
            int col = this.sensorPosCol + (positionInc[1] * i);

            if (!exploredMap.checkValidCoordinates(new int[] {row,col})) continue;

            exploredMap.getCell(new int[] {row,col}).setVisited(true);

            if(sensorVal == i)
        	{
        		exploredMap.setObstacleCell(row, col, true);
            	break;
        	}
            //Trial Run for using LR Sensor to double check for Phantom Block
            // Override previous obstacle value if front sensors detect no obstacle.
            if (exploredMap.getCell(new int[] {row,col}).isObsticle()) {
                if (id.equals("SRFL") || id.equals("SRFC") || id.equals("SRFR") || id.equals("LRL")) {
                    exploredMap.setObstacleCell(row, col, false);
                } else {
                    break;
                }
            }
        }
    }   
}
