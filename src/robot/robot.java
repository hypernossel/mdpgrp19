package robot;

import map.Map;
import tools.communicator;
import tools.communicator.COMMANDS;
import tools.utils;

import java.util.concurrent.TimeUnit;

import UI.Window;
import config.config;
import config.robotConfig;
import config.robotConfig.*;
import config.settings;


public class robot {
	private int[] robotPos; // center cell
    private DIRECTION robotDir;
    private int speed;
    private final sensor SRFrontLeft;       // north-facing front-left SR
    private final sensor SRFrontCenter;     // north-facing front-center SR
    private final sensor SRFrontRight;      // north-facing front-right SR
    private final sensor SRLeft;            // west-facing left SR
    private final sensor SRRight;           // east-facing right SR
    private final sensor LRLeft;            // west-facing left LR
    private boolean reachedGoal;
    private boolean realBot;
    private boolean initializing = false;
    
    public robot(int[] position, boolean realBot) {
        robotPos = position;
        robotDir = robotConfig.START_DIR;
        speed = robotConfig.SPEED;
        this.realBot = realBot;

        SRFrontLeft = new sensor(robotConfig.SENSOR_SHORT_RANGE_L, robotConfig.SENSOR_SHORT_RANGE_H, position[0] + 1, position[1] - 1, this.robotDir, "SRFL");
        SRFrontCenter = new sensor(robotConfig.SENSOR_SHORT_RANGE_L, robotConfig.SENSOR_SHORT_RANGE_H, position[0]+ 1, position[1], this.robotDir, "SRFC");
        SRFrontRight = new sensor(robotConfig.SENSOR_SHORT_RANGE_L, robotConfig.SENSOR_SHORT_RANGE_H, position[0] + 1, position[1] + 1, this.robotDir, "SRFR");
        SRLeft = new sensor(robotConfig.SENSOR_SHORT_RANGE_L, robotConfig.SENSOR_SHORT_RANGE_H, position[0] + 1, position[1] - 1, findNewDirection(MOVEMENT.LEFT), "SRL");
        SRRight = new sensor(robotConfig.SENSOR_SHORT_RANGE_L, robotConfig.SENSOR_SHORT_RANGE_H, position[0] + 1, position[1] + 1, findNewDirection(MOVEMENT.RIGHT), "SRR");
        LRLeft = new sensor(robotConfig.SENSOR_LONG_RANGE_L, robotConfig.SENSOR_LONG_RANGE_H, position[0], position[1] - 1, findNewDirection(MOVEMENT.LEFT), "LRL");
    }
    
    /*** Set the Robot Position ***/
    public void setPos(int[] position) { this.robotPos = position;}
    
    public void setRealRun(boolean state) {this.realBot = state;}
    
    public int[] getRobotPos() { return this.robotPos;}
    
    public void setRobotDirection(DIRECTION dir) { this.robotDir = dir;}
    
    public void setRobotSpeed (int newspeed) { this.speed = newspeed;}
    
    public boolean getReal() {return this.realBot;}
    
    public DIRECTION getRobotDirection() {return this.robotDir;}
    
    public void setInitializing(boolean state) {this.initializing = state;}
    
    public boolean getInitializing() {return this.initializing;}
    
    private void updateReachGoal()
    {
    	if(this.robotPos[0] == config.GOAL_LOC[0]
    	   && this.robotPos[1] == config.GOAL_LOC[1])
    	{
    		this.reachedGoal = true;
    	}
    }
    
    public boolean getReachGoalStatus() {return this.reachedGoal;}
    
    public void robotMovement(MOVEMENT move, boolean sendToAndroid)
    {
    	if (!realBot) {
            // Emulate real movement by pausing execution.
            try {
                TimeUnit.MILLISECONDS.sleep(speed);
            } catch (InterruptedException e) {
                System.out.println("Something went wrong in Robot.move()!");
            }
        }
    	
    	switch(move)
    	{
	    	case FORWARD:
	    		switch(robotDir)
	    		{
	    		case NORTH:
	    			robotPos[0]++;
	    			break;
	    		case EAST:
	    			robotPos[1]++;
	    			break;
	    		case SOUTH:
	    			robotPos[0]--;
	    			break;
	    		case WEST:
	    			robotPos[1]--;
	    			break;
	    		}
	    		break;
	    	case BACKWARD:
	    		switch(robotDir)
	    		{
	    		case NORTH:
	    			robotPos[0]--;
	    			break;
	    		case EAST:
	    			robotPos[1]--;
	    			break;
	    		case SOUTH:
	    			robotPos[0]++;
	    			break;
	    		case WEST:
	    			robotPos[1]++;
	    			break;
	    		}
	    		break;
	    	case BURST_2:
	    		switch(robotDir)
	    		{
	    		case NORTH:
	    			robotPos[0]+=2;
	    			break;
	    		case EAST:
	    			robotPos[1]+=2;
	    			break;
	    		case SOUTH:
	    			robotPos[0]-=2;
	    			break;
	    		case WEST:
	    			robotPos[1]-=2;
	    			break;
	    		}
	    		break;
	    	case RIGHT:
	    	case LEFT:
	    		robotDir = findNewDirection(move);
	    		break;
	    	case CALIBRATION:
	    		break;
	    	default:
                System.out.println("Error in Robot.move()!");
                break;
	    	}
    	
    	if(realBot)
    		sendMovementCommand(move,sendToAndroid);
    	else {
    		//Print Robot Status
    		String output = "<html>Robot Position : " + this.getRobotPos()[0] + ", " + this.getRobotPos()[1] + "<br>"
    				+ "Robot Facing Direction : " + this.getRobotDirection() + "<br>"
    						+ "Robot Movement : " + MOVEMENT.print(move) + "</html>";
    		
    		//Window.labelStatus.setText(output);
    	}
    	updateReachGoal();
    }
    
    /**
     * Overloaded method that calls this.move(MOVEMENT m, boolean sendMoveToAndroid = true).
     */
    public void robotMovement(MOVEMENT m) {
        this.robotMovement(m, true);
    }
    
    /**
     * Sends a number instead of 'F' for multiple continuous forward movements.
     */
    public void moveForwardMultiple(int count,boolean fastest) {
        if (count == 1 && !fastest) {
        	robotMovement(MOVEMENT.FORWARD);
        }else {
            switch (robotDir) {
                case NORTH:
                    robotPos[0] += count;
                    break;
                case EAST:
                    robotPos[1] += count;
                    break;
                case SOUTH:
                    robotPos[0] += count;
                    break;
                case WEST:
                    robotPos[1] += count;
                    break;
            }
        }
        //communicator.getCommunicator().sendMsg(this.getRobotPos()[0] + "," + this.getRobotPos()[1] + "," + DIRECTION.print(this.getRobotDirection()), COMMANDS.F_BOT_POS,true);
    }
    
    /**
     * Uses the current direction of the robot and the given movement to find the new direction of the robot.
     */
    private DIRECTION findNewDirection(MOVEMENT m) {
        if (m == MOVEMENT.RIGHT) {
            return DIRECTION.getNextDirection(robotDir);
        } else {
            return DIRECTION.getPrevDirection(robotDir);
        }
    }
    
    private void sendMovementCommand(MOVEMENT m,boolean sendToAndroid)
    {
    	communicator comm = communicator.getCommunicator();
    	comm.sendMsg(MOVEMENT.print(m) + "", COMMANDS.INSTRUCTIONS,false);
    }
    
    
    /**
     * Sets the sensors' position and direction values according to the robot's current position and direction.
     */
    public void setSensors() {
        switch (robotDir) {
            case NORTH:
                SRFrontLeft.setSensor(robotPos[0] + 1, robotPos[1] - 1, this.robotDir);
                SRFrontCenter.setSensor(robotPos[0] + 1, robotPos[1], this.robotDir);
                SRFrontRight.setSensor(robotPos[0] + 1, robotPos[1] + 1, this.robotDir);
                SRLeft.setSensor(robotPos[0] + 1, robotPos[1] - 1, findNewDirection(MOVEMENT.LEFT));
                LRLeft.setSensor(robotPos[0], robotPos[1] - 1, findNewDirection(MOVEMENT.LEFT));
                SRRight.setSensor(robotPos[0] + 1, robotPos[1] + 1, findNewDirection(MOVEMENT.RIGHT));
                break;
            case EAST:
                SRFrontLeft.setSensor(robotPos[0] + 1, robotPos[1] + 1, this.robotDir);
                SRFrontCenter.setSensor(robotPos[0], robotPos[1] + 1, this.robotDir);
                SRFrontRight.setSensor(robotPos[0] - 1, robotPos[1] + 1, this.robotDir);
                SRLeft.setSensor(robotPos[0] + 1, robotPos[1] + 1, findNewDirection(MOVEMENT.LEFT));
                LRLeft.setSensor(robotPos[0] + 1, robotPos[1], findNewDirection(MOVEMENT.LEFT));
                SRRight.setSensor(robotPos[0] - 1, robotPos[1] + 1, findNewDirection(MOVEMENT.RIGHT));
                break;
            case SOUTH:
                SRFrontLeft.setSensor(robotPos[0] - 1, robotPos[1] + 1, this.robotDir);
                SRFrontCenter.setSensor(robotPos[0] - 1, robotPos[1], this.robotDir);
                SRFrontRight.setSensor(robotPos[0] - 1, robotPos[1] - 1, this.robotDir);
                SRLeft.setSensor(robotPos[0] - 1, robotPos[1] + 1, findNewDirection(MOVEMENT.LEFT));
                LRLeft.setSensor(robotPos[0], robotPos[1] + 1, findNewDirection(MOVEMENT.LEFT));
                SRRight.setSensor(robotPos[0] - 1, robotPos[1] - 1, findNewDirection(MOVEMENT.RIGHT));
                break;
            case WEST:
                SRFrontLeft.setSensor(robotPos[0] - 1, robotPos[1] - 1, this.robotDir);
                SRFrontCenter.setSensor(robotPos[0], robotPos[1] - 1, this.robotDir);
                SRFrontRight.setSensor(robotPos[0] + 1, robotPos[1] - 1, this.robotDir);
                SRLeft.setSensor(robotPos[0] - 1, robotPos[1] - 1, findNewDirection(MOVEMENT.LEFT));
                LRLeft.setSensor(robotPos[0] - 1, robotPos[1], findNewDirection(MOVEMENT.LEFT));
                SRRight.setSensor(robotPos[0] + 1, robotPos[1] - 1, findNewDirection(MOVEMENT.RIGHT));
                break;
        }

    }
    
    /**
     * Calls the .sense() method of all the attached sensors and stores the received values in an integer array.
     *
     * @return [SRFrontLeft, SRFrontCenter, SRFrontRight, SRLeft, SRRight, LRLeft]
     */
    public int[] sense(Map explorationMap,Map realMap) {
        int[] result = new int[6];
        
        if(!realBot)
        {
        	result[0] = SRFrontLeft.sense(explorationMap,realMap);
            result[1] = SRFrontCenter.sense(explorationMap,realMap);
            result[2] = SRFrontRight.sense(explorationMap,realMap);
            result[3] = SRLeft.sense(explorationMap,realMap);
            result[4] = SRRight.sense(explorationMap,realMap);
            result[5] = LRLeft.sense(explorationMap,realMap);
            System.out.println("NOT REAL ROBOT SENSOR");
        }else
        {
        	
        	if(initializing) return null;
        	
        	communicator comm = communicator.getCommunicator();
        	String msg = comm.recvMsg();
        	if(msg.substring(0, 2).equals("s_")){
        		System.out.println("msg : " + msg);
        		String sensor_dat[] = msg.split("_")[1].split(",");
            	for(int i=0; i < result.length; i++)
        			result[i] = Integer.parseInt(sensor_dat[i]);
        	}
        	
        	SRFrontLeft.senseReal(explorationMap, result[0]+1);
            SRFrontCenter.senseReal(explorationMap, result[1]+1);
            SRFrontRight.senseReal(explorationMap, result[2]+1);
            SRRight.senseReal(explorationMap, result[3]+1);
            SRLeft.senseReal(explorationMap, result[4]+1);
            LRLeft.senseReal(explorationMap, result[5]+1);
        }
        
        
        String[] map = new String[2];
        
        map[0] = utils.generateMDF(explorationMap);
        map[1] = utils.generateMDF2(explorationMap);
        
	   	communicator.getCommunicator().sendMsg(map[0] + "," + map[1] + ";" + this.getRobotPos()[0] + "," + this.getRobotPos()[1] + "," + DIRECTION.print(this.getRobotDirection()), COMMANDS.MAP,true);
        return result;
    }

    
    
    

}
