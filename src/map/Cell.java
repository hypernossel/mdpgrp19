package map;

import config.config;

public class Cell {
	int row;
	int col;
	
	boolean isObsticle;
	boolean isVisited;
	boolean isVirtualWall;
	boolean isWaypoint;
	boolean fogofwarexplored;
	boolean hasArrow;
	boolean trail;
	
	int id;
	
	public Cell(int row,int col)
	{
		this.row = row;
		this.col = col;
		this.id = row * 15 + col;
	}
	
	
	public int getColumn() { return this.col; }
	public int getRow() { return this.row; }
	
	public int getID() {return this.id;}
	
	public boolean isTrail() {return this.trail;}
	
	public void setObsticle(boolean state) { this.isObsticle = state;}
	public void setVisited(boolean state) { this.isVisited = state;}
	
	public boolean getHasArrow() {return this.hasArrow;}
	
	public void setArrow(boolean state) {this.hasArrow = state;}
	
	public void setWaypoint(boolean state) {this.isWaypoint = state;}
	
	public boolean isWayPoint() {return this.isWaypoint;}
	
	public void setTrail(boolean state) {this.trail = state;}
	
	public void setfogofwarExplored(boolean state) {this.fogofwarexplored = state;}
	public void setVirtualWall(boolean state) { 
		if(state)
			this.isVirtualWall = state;
		else
		{
			if(row != 0 && row != config.GRID_ROW -1 && col != 0 && col != config.GRID_COL-1)
				this.isVirtualWall = false;
		}
		
	}
	
	public boolean isVirtualWall() { return this.isVirtualWall; }
	public boolean isVisited() { return this.isVisited;}
	public boolean isObsticle() { return this.isObsticle;}
	public boolean isFogOfwarExplored() {return this.fogofwarexplored; }
	
}
