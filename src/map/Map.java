package map;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;

import javax.swing.JPanel;

import config.GraphicsConstants;
import config.config;
import config.robotConfig;
import robot.robot;

public class Map extends JPanel {
	
   private Cell[][] cellMap;
   
   private robot bot;
   
   private boolean fogOfWar = true;
   
   public Map(robot bot) {
	   this.bot = bot;
	   
	   generateMap();
   }
   
   private void generateMap()
   {
	   cellMap = new Cell[config.GRID_ROW][config.GRID_COL];
	   for (int row = 0; row < cellMap.length; row++) {
           for (int col = 0; col < cellMap[0].length; col++) {
        	   cellMap[row][col] = new Cell(row, col);

               // Set the virtual walls of the arena
               if (row == 0 || col == 0 || row == config.GRID_ROW - 1 || col == config.GRID_COL - 1) {
            	   cellMap[row][col].setVirtualWall(true);
               }
           }
       }
   }
   
   /**
    * Returns true if the row and column values are valid.
    */
   public boolean checkValidCoordinates(int[] position) {
       return position[0] >= 0 && position[1] >= 0 && position[0] < config.GRID_ROW && position[1] < config.GRID_COL;
   }

   /**
    * Returns true if the row and column values are in the start zone.
    */
   public boolean inStartZone(int row, int col) {
	   return row >= 0 && row <= 2 && col >= 0 && col <= 2;
   }

   
   /**
    * Returns true if the row and column values are in the goal zone.
    */
   public boolean inGoalZone(int row, int col) {
	   return (row <= config.GOAL_LOC[0] + 1 && row >= config.GOAL_LOC[0] - 1 && col <= config.GOAL_LOC[1] + 1 && col >= config.GOAL_LOC[1] - 1);
   }
   
   /**
    * Returns a particular cell in the grid.
    */
   public Cell getCell(int[] position) {
       return cellMap[position[0]][position[1]];
   }
   
   /**
    * Returns true if a cell is an obstacle.
    */
   public boolean isObstacleCell(int[] position) {
       return cellMap[position[0]][position[1]].isObsticle();
   }
   
   /**
    * Returns true if a cell is a virtual wall.
    */
   public boolean isVirtualWallCell(int[] position) {
       return cellMap[position[0]][position[1]].isVirtualWall();
   }
   
   /**
    * Sets all cells in the grid to an explored state.
    */
   public void setAllExplored() {
	   for (int row = 0; row < cellMap.length; row++) {
           for (int col = 0; col < cellMap[0].length; col++) {
               cellMap[row][col].setVisited(true);
           }
       }
   }
   
   /**
    * Sets all cells in the grid to an unexplored state except for the START & GOAL zone.
    */
   public void setAllUnexplored() {
       for (int row = 0; row < cellMap.length; row++) {
           for (int col = 0; col < cellMap[1].length; col++) {
               if (inStartZone(row, col) || inGoalZone(row, col)) {
                   cellMap[row][col].setVisited(true);
               } else {
            	   if(fogOfWar) {
            		   if(!cellMap[row][col].isFogOfwarExplored())
            			   cellMap[row][col].setVisited(false);
            	   }else	   
            		   cellMap[row][col].setVisited(false);
               }
           }
       }
   }
   
   public void clearMap()
   {
	   generateMap();
	   
	   bot.setPos(new int[] {robotConfig.START_ROW,robotConfig.START_COL});
   }
   
   public void hideFog()
   {
	   fogOfWar = !fogOfWar;
	   
	   if(!fogOfWar)
		   setAllExplored();
	   else
		   setAllUnexplored();
	   
	   this.repaint();
   }
   
   public ArrayList<Integer> findWalls()
   {
	   ArrayList<Integer> walls = new ArrayList<Integer>();
	   	
	   	for(int i=0; i< cellMap.length; i++)
	   	{
	   		for(int j=0; j<cellMap[0].length; j++)
	   		{
	   			if((cellMap[i][j]).isObsticle())
	   			{ 
	   				walls.add(i * 15 + j);
	   			}
	   			/*if((cellMap[i][j].isVisited == false)) {
	   				walls.add(i*15+j);
	   			}*/
	   		}
	   	}
	   	return walls;
	   }
   
   public ArrayList<Cell> findUnexplored(int gridRadius)
   {
	   gridRadius = (gridRadius == -1) ? 1 : gridRadius;
	   ArrayList<Cell> unexploredCells = new ArrayList<Cell>();
	   for(int row=0; row<cellMap.length; row++)
	   {
		   for(int col=0; col<cellMap[0].length; col++)
		   {
			   if(!(cellMap[row][col]).isVisited())
			   {
				   if(row + gridRadius < 20 && row - gridRadius > 0) {
					   if(!cellMap[row-gridRadius][col].isVirtualWall() && !cellMap[row-gridRadius][col].isObsticle())
						   unexploredCells.add(cellMap[row-gridRadius][col]);
					   else if(!cellMap[row+gridRadius][col].isVirtualWall() && !cellMap[row+gridRadius][col].isObsticle())
						   unexploredCells.add(cellMap[row+gridRadius][col]);
				   }
				   if(col + gridRadius < 15 && col - gridRadius > 0) {	   
					   if(!cellMap[row][col-gridRadius].isVirtualWall() && !cellMap[row][col-gridRadius].isObsticle())
						   unexploredCells.add(cellMap[row][col-gridRadius]);
					   else if(!cellMap[row][col+gridRadius].isVirtualWall() && !cellMap[row][col+gridRadius].isObsticle())
						   unexploredCells.add(cellMap[row][col+gridRadius]); 
				   }
			   }  
		   }
	   }	   return unexploredCells;
   }
   
   public void setWayPoint(int row,int col, boolean waypoint) {
	   if(cellMap[row][col].isObsticle() && cellMap[row][col].isVirtualWall()) return;
	   
	   cellMap[row][col].setWaypoint(waypoint);
   }
   
   public ArrayList<Cell> findWaypoints(){
	     ArrayList<Cell> waypoints = new ArrayList<Cell>();
	     for(int i = 0 ; i < cellMap.length; i ++ ) {
	       for(int j = 0 ; j < cellMap[0].length; j ++) {
	         if(cellMap[i][j].isWaypoint) {
	           waypoints.add(cellMap[i][j]);
	         }
	       }
	     }
	     
	     return waypoints;
	   }
   
   /**
    * Sets a cell as an obstacle and the surrounding cells as virtual walls or resets the cell and surrounding
    * virtual walls.
    */
   public void setObstacleCell(int row, int col, boolean obstacle) {
       if (obstacle && (inStartZone(row, col) || inGoalZone(row, col)))
           return;
       
       if(cellMap[row][col].isWayPoint())
    	   cellMap[row][col].setWaypoint(false);
    	   

       cellMap[row][col].setObsticle(obstacle);
       cellMap[row][col].setVisited(true);

       if (row >= 1) {
           cellMap[row - 1][col].setVirtualWall(obstacle);            // bottom cell

           if (col < config.GRID_COL - 1) {
               cellMap[row - 1][col + 1].setVirtualWall(obstacle);    // bottom-right cell
           }

           if (col >= 1) {
               cellMap[row - 1][col - 1].setVirtualWall(obstacle);    // bottom-left cell
           }
       }

       if (row < config.GRID_ROW - 1) {
           cellMap[row + 1][col].setVirtualWall(obstacle);            // top cell

           if (col < config.GRID_COL - 1) {
               cellMap[row + 1][col + 1].setVirtualWall(obstacle);    // top-right cell
           }

           if (col >= 1) {
               cellMap[row + 1][col - 1].setVirtualWall(obstacle);    // top-left cell
           }
       }

       if (col >= 1) {
           cellMap[row][col - 1].setVirtualWall(obstacle);            // left cell
       }

       if (col < config.GRID_COL - 1) {
           cellMap[row][col + 1].setVirtualWall(obstacle);            // right cell
       }
   }
   
   /**
    * Returns true if the given cell is out of bounds or an obstacle.
    */
   public boolean getIsObstacleOrWall(int[] position) {
       return !checkValidCoordinates(position) || getCell(position).isObsticle();
   }
   
   
   public Cell[][] getMap(){return this.cellMap;}
   
   
   /**
    * Overrides JComponent's paintComponent() method. It creates a two-dimensional array of _DisplayCell objects
    * to store the current map state. Then, it paints square cells for the grid with the appropriate colors as
    * well as the robot on-screen.
    */
   public void paintComponent(Graphics g) {
       // Create a two-dimensional array of _DisplayCell objects for rendering.
       _DisplayCell[][] _mapCells = new _DisplayCell[config.GRID_ROW][config.GRID_COL];
       for (int mapRow = 0; mapRow < config.GRID_ROW; mapRow++) {
           for (int mapCol = 0; mapCol < config.GRID_COL; mapCol++) {
               _mapCells[mapRow][mapCol] = new _DisplayCell(mapCol * GraphicsConstants.CELL_SIZE, mapRow * GraphicsConstants.CELL_SIZE, GraphicsConstants.CELL_SIZE);
           }
       }

       // Paint the cells with the appropriate colors.
       for (int mapRow = 0; mapRow < config.GRID_ROW; mapRow++) {
           for (int mapCol = 0; mapCol < config.GRID_COL; mapCol++) {
               Color cellColor;

               if (inStartZone(mapRow, mapCol))
                   cellColor = GraphicsConstants.C_START;
               else if (inGoalZone(mapRow, mapCol))
                   cellColor = GraphicsConstants.C_GOAL;
               else {
                   if (!cellMap[mapRow][mapCol].isVisited())
                       cellColor = GraphicsConstants.C_UNEXPLORED;
                   else if (cellMap[mapRow][mapCol].isObsticle())
                       cellColor = GraphicsConstants.C_OBSTACLE;
                   else if(cellMap[mapRow][mapCol].isVirtualWall())
                   	   cellColor = GraphicsConstants.C_VIRTUAL_WALL;
                   else if(cellMap[mapRow][mapCol].isWayPoint())
                	   cellColor = GraphicsConstants.C_WAYPOINT;
                   else if(cellMap[mapRow][mapCol].isTrail())
                	   cellColor = GraphicsConstants.C_TRAIL;
                   else
                       cellColor = GraphicsConstants.C_FREE;
               }

               g.setColor(cellColor);
               g.fillRect(_mapCells[mapRow][mapCol].cellX + GraphicsConstants.MAP_X_OFFSET, _mapCells[mapRow][mapCol].cellY, _mapCells[mapRow][mapCol].cellSize, _mapCells[mapRow][mapCol].cellSize);

           }
       }

       // Paint the robot on-screen.
       g.setColor(GraphicsConstants.C_ROBOT);
       int r = bot.getRobotPos()[0];
       int c = bot.getRobotPos()[1];
       g.fillOval((c - 1) * GraphicsConstants.CELL_SIZE + GraphicsConstants.ROBOT_X_OFFSET + GraphicsConstants.MAP_X_OFFSET, GraphicsConstants.MAP_H - (r * GraphicsConstants.CELL_SIZE + GraphicsConstants.ROBOT_Y_OFFSET), GraphicsConstants.ROBOT_W, GraphicsConstants.ROBOT_H);

       // Paint the robot's direction indicator on-screen.
       g.setColor(GraphicsConstants.C_ROBOT_DIR);
       robotConfig.DIRECTION d = bot.getRobotDirection();
       switch (d) {
           case NORTH:
               g.fillOval(c * GraphicsConstants.CELL_SIZE + 10 + GraphicsConstants.MAP_X_OFFSET, GraphicsConstants.MAP_H - r * GraphicsConstants.CELL_SIZE - 15, GraphicsConstants.ROBOT_DIR_W, GraphicsConstants.ROBOT_DIR_H);
               break;
           case EAST:
               g.fillOval(c * GraphicsConstants.CELL_SIZE + 35 + GraphicsConstants.MAP_X_OFFSET, GraphicsConstants.MAP_H - r * GraphicsConstants.CELL_SIZE + 10, GraphicsConstants.ROBOT_DIR_W, GraphicsConstants.ROBOT_DIR_H);
               break;
           case SOUTH:
               g.fillOval(c * GraphicsConstants.CELL_SIZE + 10 + GraphicsConstants.MAP_X_OFFSET, GraphicsConstants.MAP_H - r * GraphicsConstants.CELL_SIZE + 35, GraphicsConstants.ROBOT_DIR_W, GraphicsConstants.ROBOT_DIR_H);
               break;
           case WEST:
               g.fillOval(c * GraphicsConstants.CELL_SIZE - 15 + GraphicsConstants.MAP_X_OFFSET, GraphicsConstants.MAP_H - r * GraphicsConstants.CELL_SIZE + 10, GraphicsConstants.ROBOT_DIR_W, GraphicsConstants.ROBOT_DIR_H);
               break;
       }
   }

   private class _DisplayCell {
       public final int cellX;
       public final int cellY;
       public final int cellSize;

       public _DisplayCell(int borderX, int borderY, int borderSize) {
           this.cellX = borderX + GraphicsConstants.CELL_LINE_WEIGHT;
           this.cellY = GraphicsConstants.MAP_H - (borderY - GraphicsConstants.CELL_LINE_WEIGHT);
           this.cellSize = borderSize - (GraphicsConstants.CELL_LINE_WEIGHT * 2);
       }
   }
   
  
	
}
